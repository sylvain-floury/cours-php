<?php
declare(strict_types = 1);

namespace koan\type;

use PHPUnit\Framework\TestCase;

if (!defined('___')) define('___', null);

class TypeHintingTest extends TestCase
{
    /**
     * @test
     * @testdox la méthode n'accepte que des paramètres de type entier
     */
    public function shouldOnlyAcceptIntegerAsParameter()
    {
        $this->expectException(\TypeError::class);
        ArithmeticOperations::addInt("1", 2.0);
        self::assertThat(ArithmeticOperations::addInt(2, 2), self::equalTo(4));
    }

    /**
     * @test
     * @testdox la méthode n'accepte que des paramètres de type réel
     */
    public function shouldOnlyAcceptFloatAsParameter()
    {
        $this->expectException(\TypeError::class);
        ArithmeticOperations::addFloat("1", 2.0);
        self::assertThat(ArithmeticOperations::addFloat(2, 2.0), self::equalTo(4));
    }

    /**
     * @test
     * @testdox la méthode n'accepte que des paramètres de type string
     */
    public function shouldOnlyAcceptStringAsParameter()
    {
        $this->expectException(\TypeError::class);
        StringOperation::concat(1, 2.0);
        self::assertThat(StringOperation::concat("foo", "bar"), self::equalTo("foobar"));
    }

    /**
     * @test
     * @testdox la méthode 'add' accepte uniquement des instances de la classe 'Person'
     */
    public function shouldAcceptOnlyPersonClass()
    {
        $personStore = new PersonStore();

        $personStore->add(___);

        self::assertThat($personStore->count(), self::equalTo(1));
    }

    /**
     * @test
     * @testdox le polymorphisme va permettre d'utiliser la méthode 'add'
     */
    public function shouldAlsoAcceptSubClass()
    {
        $personStore = new PersonStore();

        $personStore->add(new Developer('Robert', 'Paulson'));

        self::assertThat($personStore->count(), self::equalTo(1));
    }

    /**
     * @test
     * @testdox la methode 'first' doit retourner une erreur si l'élément retourné n'est pas une instance de 'Person'
     *
     * @see https://www.doctrine-project.org/projects/doctrine-collections/en/current/index.html#first
     */
    public function shouldReturnPersonClass()
    {
        $personStore = new PersonStore();

        $this->expectException(\TypeError::class);
        $personStore->first();
    }

    /**
     * @test
     * @testdox la méthode 'addIfExists' autorise les instances de Person ou null
     *
     * @see https://www.doctrine-project.org/projects/doctrine-collections/en/current/index.html#add
     */
    public function shouldAcceptParameterOrNull()
    {
        $personStore = new PersonStore();

        $personStore->addIfExists(new Person('John', 'Doe'));
        $personStore->addIfExists(null);
        self::assertThat($personStore->count(), self::equalTo(1));
        $this->expectException(\TypeError::class);
        $personStore->addIfExists("error");
    }

    /**
     * @test
     * @testdox la méthode 'last' doit retourner null si la liste est vide
     *
     * @see https://www.doctrine-project.org/projects/doctrine-collections/en/current/index.html#last
     */
    public function shouldReturnNullIfStoreIsEmpty()
    {
        $personStore = new PersonStore();

        self::assertThat($personStore->last(), self::isNull());
    }

    /**
     * @test
     * @testdox la méthode 'last' doit retourner une instance de Person si la liste n'est pas vide
     */
    public function shouldReturnPersonIfStoreHasAtLeastOnePerson()
    {
        $personStore = new PersonStore();
        $personStore->add(new Person('John', 'Doe'));

        self::assertThat($personStore->last(), self::isInstanceOf(Person::class));
    }
}
