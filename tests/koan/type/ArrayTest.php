<?php

namespace koan\type;

use PHPUnit\Framework\TestCase;

if (!defined('___')) define('___', null);

class ArrayTest extends TestCase
{
    /**
     * @test
     * @testdox un tableau est défini avec des crochets [];
     */
    public function shouldDefineArrayWithBracket()
    {
        $shoppingList = ___;

        self::assertIsArray($shoppingList);
        self::assertThat($shoppingList, self::countOf(2));
    }

    /**
     * @test
     * @testdox définition d'un tableau vide
     */
    public function shouldBeEmptyArray()
    {
        self::assertThat(___, self::isEmpty());
    }

    /**
     * @test
     * @testdox un tableau associatif est utile pour stocker des paires de clé + valeur
     */
    public function shouldDefineAssociativeArray()
    {
        $shoppingList = ___;

        self::assertIsArray($shoppingList);
        self::assertThat($shoppingList, self::countOf(2));
        self::assertThat($shoppingList, self::arrayHasKey('foo'));
        self::assertThat($shoppingList['foo'], self::equalTo(42));
    }
}