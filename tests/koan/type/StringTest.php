<?php

namespace koan\type;

use PHPUnit\Framework\TestCase;

if (!defined('___')) define('___', null);

class StringTest extends TestCase
{
    /**
     * @test
     * @testdox les simple quotes définissent une chaine de caractères
     */
    public function shouldDefineStringWithSimpleQuote()
    {
        self::assertThat(___, self::isType('string'));
    }

    /**
     * @test
     * @testdox les double quotes définissent une chaine de caractères
     */
    public function shouldDefineStringWithDoubleQuote()
    {
        self::assertThat(___, self::isType('string'));
    }

    /**
     * @test
     * @testdox définition d'une chaine vide.
     */
    public function shouldBeEmptyString()
    {
        $emptyString = ___;
        self::assertThat($emptyString, self::logicalNot(self::isNull()));
        self::assertThat($emptyString, self::isType('string'));
        self::assertThat($emptyString, self::isEmpty());
    }

    /**
     * @test
     * @testdox les doubles quotes permettent l’interpolation de chaînes
     *
     * @see https://en.wikipedia.org/wiki/String_interpolation
     */
    public function shouldInterpolateVariableInString()
    {
        $playerOne = 'Giseline';
        $playerTwo = 'Madeline';
        self::assertThat(___, self::equalTo('Hello Giseline!'));
        self::assertThat(___, self::equalTo('Hello _Madeline_'));
    }

    /**
     * @test
     * @testdox les simples quotes ne permettent pas l’interpolation de chaînes
     */
    public function shouldNotInterpolateVariableInString()
    {
        $name = 'Giseline';
        self::assertThat(___, self::equalTo('Hello $name!'));
    }

    /**
     * @test
     * @testdox il est possible de retouner une chaine formatée.
     *
     * @see https://www.php.net/manual/fr/function.sprintf.php#refsect1-function.sprintf-parameters
     */
    public function shouldFormatString()
    {
        $format = ___;
        self::assertThat(sprintf($format, 'Paul', 2, 5), self::equalTo('Paul is selling 2 boxes for 05.00 €'));
    }

    /**
     * @test
     * @testdox effectue la concaténation des chaines de caractères
     */
    public function shouldConcatTwoStrings()
    {
        $firstString = "Hello ";
        $secondString = "World!";

        self::assertThat($firstString . $secondString, self::equalTo(___));
    }

    /**
     * @test
     * @testdox effectue la concaténation et l'assignation en même temps
     */
    public function shouldAssignAndConcatAtTheSameTime()
    {
        $string = "foo";
        $string .= "bar";

        self::assertThat($string, self::equalTo(___));
    }
}