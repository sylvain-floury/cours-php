<?php
declare(strict_types = 1);

namespace koan\type;

use PHPUnit\Framework\TestCase;

if (!defined('___')) define('___', null);

class PHP7_4Test extends TestCase
{
    /**
     * @test
     * @testdox les attributs doivent être de type chaine de caractères
     */
    public function shouldOnlyAcceptStringAsAttributes() {
        $this->expectException(\TypeError::class);
        $person = new PersonWithAttributes();
        $person->firstName = 'John';
        $person->lastName = 2;
    }

    /**
     * @test
     * @testdox L'opérateur Null coalescing permet de définir une valeur par défaut.
     */
    public function shouldDefineDefaultValue() {
        $userNames = [];
        $userNames['John'] ??= 'Doe';
        self::assertThat($userNames['John'], self::equalTo(___));
    }
}
