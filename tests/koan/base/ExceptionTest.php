<?php
declare(strict_types = 1);

namespace koan\base;

if (!defined('___')) define('___', null);

use Exception;
use PHPUnit\Framework\TestCase;

class ExceptionTest extends TestCase
{
    /**
     * @test
     * @testdox lève une exception
     */
    public function shouldRiseException()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('An exception has occur.');
        ___;
    }

    /**
     * @test
     * @testdox doit intercepter l'exception levée
     */
    public function shouldCatchException()
    {
        try {
            $name = 'John Doe';
            throw new \InvalidArgumentException('Wrong type parameter');
            $name = 'Jane Doe';
        }
        catch (____ $e) {
            self::assertThat($e, self::isInstanceOf(\InvalidArgumentException::class));
        }

        self::assertThat($name, self::equalTo(___));
    }

    /**
     * @test
     * @testdox bloc de code exécuté même si une exception intervient
     */
    public function shouldHappenEvenWithAnException()
    {
        $name = 'John Doe';
        
        try {
            throw new \InvalidArgumentException('Wrong type parameter');
        }
        catch (\InvalidArgumentException $e) {
            self::assertThat($e, self::isInstanceOf(\InvalidArgumentException::class));
        }
        finally {
            $name = 'Jane Doe';
        }

        self::assertThat($name, self::equalTo(___));
    }

    /**
     * @test
     * @testdox le mécanisme d'héritage fonctionne avec les exceptions
     */
    public function shouldCatchSubclassedException()
    {
        $store = new TransactionStore();
        $store->add(new Transaction(new \DateTime("2019-09-20"), "test", 10.0));

        try {
            $store->get(1);
        }
        catch (____ $e){
            self::assertThat($e, self::isInstanceOf(\RuntimeException::class));
            self::assertThat($e, self::isInstanceOf(\OutOfBoundsException::class));
            self::assertThat($e->getMessage(), self::equalTo('Index 1 is out of bounds'));
        }
    }

    /**
     * @test
     * @testdox les erreurs peuvent également être attrapées.
     */
    public function shouldCatchError()
    {
        try {
            Operator::divide(10, "2");
        }
        catch (____ $er) {
            self::assertThat($er, self::isInstanceOf(\Error::class));
            self::assertThat($er->getMessage(), self::stringContains('Argument 2 passed to koan\base\Operator::divide() must be of the type int, string given'));
        }
    }
}