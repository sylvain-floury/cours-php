<?php

namespace koan\base;

if (!defined('___')) define('___', null);

use PHPUnit\Framework\TestCase;

class AssertionTest extends TestCase
{
    /**
     * @test
     * @testdox vérifie que l'assertion est "true"
     */
    public function shouldBeTrue()
    {
        self::assertThat(___, self::isTrue());
    }

    /**
     * @test
     * @testdox vérifie que l'assertion est "false"
     */
    public function shouldBeFalse()
    {
        self::assertThat(___, self::isFalse());
    }

    /**
     * @test
     * @testdox vérifie l'égalité entre la valeur et l'attendu
     */
    public function shouldBeEqualTo2()
    {
        self::assertThat(___, self::equalTo(2));
    }

    /**
     * @test
     * @testdox assigne une valeur à une variable
     */
    public function shouldAssignValue()
    {
        $number = ___;

        self::assertThat($number, self::equalTo(5));
    }

    /**
     * @test
     * @testdox vérifie l'égalité entre 2 éléments
     */
    public function shouldCheckIsEqual()
    {
        $number = ___;

        self::assertThat(5 == $number, self::isTrue());
    }

    /**
     * @test
     * @testdox vérifie que 2 éléments ne sont pas égaux.
     */
    public function shouldCheckIsNotEqual()
    {
        $number = ___;

        self::assertThat($number, self::logicalNot(self::isNull()));
        self::assertThat(5 != $number, self::isTrue());
    }

    /**
     * @test
     * @testdox vérifie l'égalité entre 2 éléments de types différents
     */
    public function shouldCheckIsEqualEvenBetweenDifferentTypes()
    {
        $number = ___;

        self::assertThat('5' == $number, self::isTrue());
    }

    /**
     * @test
     * @testdox vérifie l'inégalité entre 2 éléments de types différents
     */
    public function shouldCheckIsNotEqualEvenBetweenDifferentTypes()
    {
        $number = ___;

        self::assertThat($number, self::logicalNot(self::isNull()));
        self::assertThat('5' != $number, self::isTrue());
    }

    /**
     * @test
     * @testdox vérifie l'égalité entre 2 éléments s'ils sont du même type
     */
    public function shouldCheckIsEqualIfElementsHaveSameType()
    {
        $number = ___;

        self::assertThat('5' === $number, self::isFalse());
        self::assertThat(5 === $number, self::isTrue());
    }

    /**
     * @test
     * @testdox vérifie l'inégalité entre 2 éléments en tenant compte de leur type
     */
    public function shouldCheckIsNotEqualIfElementsHaveSameType()
    {
        $number = ___;

        self::assertThat($number, self::logicalNot(self::isNull()));
        self::assertThat(5 !== $number, self::isTrue());
        self::assertThat('5' !== $number, self::isTrue()); // be careful it's true because of the type difference
    }
}
