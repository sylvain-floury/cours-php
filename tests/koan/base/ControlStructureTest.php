<?php

namespace koan\base;

use PHPUnit\Framework\TestCase;

if (!defined('___')) define('___', null);

class ControlStructureTest extends TestCase
{
    /**
     * @test
     * @testdox execute le bloc de code si l'expression est "true"
     */
    public function shouldExecuteStatementIfExpressionIsTrue()
    {
        $value = 0;

        if ($value < 3) {
            $value = 4;
        }

        if ($value <= 4) {
            $value = 5;
        }

        self::assertThat($value, self::equalTo(___));
    }

    /**
     * @test
     * @testdox execute le bloc de code alternatif si son expression est "true"
     */
    public function shouldExecuteStatementOfAlternateExpressionIfTrue()
    {
        $value = 5;

        if ($value < 3) {
            $result = 'first expression';
        } elseif ($value >= 4) {
            $result = 'alternate expression';
        }

        self::assertThat($result, self::equalTo(___));
    }

    /**
     * @test
     * @testdox définit un bloc de code si aucune expression précédente n'est "true"
     */
    public function shouldExecuteStatementIfNoneIsTrue()
    {
        $value = 5;

        if ($value < 3) {
            $result = 'first expression';
        } elseif ($value == 4) {
            $result = 'alternate expression';
        } else {
            $result = 'otherwise';
        }


        self::assertThat($result, self::equalTo(___));
    }

    /**
     * @test
     * @testdox exécute la boucle tant que la condition est "true"
     */
    public function shouldExecuteForLoopWhileConditionalExpressionIsTrue()
    {
        $result = 0;

        $increase = function (&$a, &$b) {
            $a++;
            $b +=2 ;
        };

        for ($i = $j = 0; $i < 10 && $j < 10; $increase($i, $j)) {
            $result++;
        }

        self::assertThat($result, self::equalTo(___));
    }

    /**
     * @test
     * @testdox boucle tant qu’une condition n’est pas remplie
     */
    public function shouldIterateWhileTrue()
    {
        $result = 0;

        while ($result < 10) {
            $result++;
        }

        self::assertThat($result, self::equalTo(___));
    }

    /**
     * @test
     * @testdox itère sur tous les éléments d'une collection
     */
    public function shouldLoopOverAllArrayValues()
    {
        $numbers = array(1, 2, 3, 4);
        $result = 1;

        foreach ($numbers as $figure) {
            $result *= $figure;
        }

        self::assertThat($result, self::equalTo(___));
    }
}
