<?php

namespace koan\base;

use PHPUnit\Framework\TestCase;

if (!defined('___')) define('___', null);

class ArithmeticTest extends TestCase
{
    /**
     * @test
     * @testdox effectue la somme des nombres
     */
    public function shouldSumNumbers()
    {
        $sum = 1 + 2 + 3;
        self::assertThat($sum, self::equalTo(___));
    }

    /**
     * @test
     * @testdox effectue la soustraction des nombres
     */
    public function shouldSubtractNumbers()
    {
        $result = 4 - 2 - 1;
        self::assertThat($result, self::equalTo(___));
    }

    /**
     * @test
     * @testdox effectue la multiplication des nombres
     */
    public function shouldMultiplyNumbers()
    {
        $result = 6 * 7;
        self::assertThat($result, self::equalTo(___));
    }

    /**
     * @test
     * @testdox effectue la division des nombres
     */
    public function shouldDivideNumbers()
    {
        $result = 10 / 4;
        self::assertThat($result, self::equalTo(___));
    }

    /**
     * @test
     * @testdox reste de la division entière
     */
    public function shouldReturnRemainderOfDivision()
    {
        $modulo = 11 % 4;
        self::assertThat($modulo, self::equalTo(___));
    }

    /**
     * @test
     * @testdox retourne le resultat d’un nombre $x à la puissance $y
     */
    public function shouldReturn2Power4()
    {
        $exponent = 2 ** 4;
        self::assertThat($exponent, self::equalTo(___));
    }

    /**
     * @test
     * @testdox effectue la somme et l'assignation en même temps
     */
    public function shouldSumAndAssignAtTheSameTime()
    {
        $result = 2;
        $result += 2;
        self::assertThat($result, self::equalTo(___));
    }

    /**
     * @test
     * @testdox incrémente après l'opération
     */
    public function shouldIncrementAfterOperation()
    {
        $i = 1;

        self::assertThat($i++, self::equalTo(___));
        self::assertThat($i, self::equalTo(___));
    }

    /**
     * @test
     * @testdox incrémente avant l'opération
     */
    public function shouldIncrementBeforeOperation()
    {
        $i = 1;

        self::assertThat(++$i, self::equalTo(___));
    }

    /**
     * @test
     * @testdox décrémente après l'opération
     */
    public function shouldDecrementAfterOperation()
    {
        $i = 1;

        self::assertThat($i--, self::equalTo(0));
        self::assertThat($i, self::equalTo(___));
    }
}
