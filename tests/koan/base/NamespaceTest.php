<?php


namespace koan\base;

if (!defined('___')) define('___', null);

use PHPUnit\Framework\TestCase;

class NamespaceTest extends TestCase
{

    /**
     * @test
     * @testdox instanciation de la classe à partir de son nom absolu.
     */
    public function shouldInstantiateClassFromFullQualifiedName()
    {
        $john = ___;
        self::assertThat(get_class($john), self::equalTo('koan\base\ns\KoanBasicPerson')); // This assertion is for learning purpose only. Use Clazz::class instead.
        self::assertThat($john->firstName, self::equalTo('John'));
        self::assertThat($john->lastName, self::equalTo('Doe'));
    }

    /**
     * @test
     * @testdox instanciation de la classe uniquement à partir du nom.
     */
    public function shouldInstantiateClassFromClassNameOnly()
    {
        $john = new KoanPerson('John', 'Doe');
        self::assertThat(get_class($john), self::equalTo('koan\base\ns\KoanPerson')); // This assertion is for learning purpose only. Use Clazz::class instead.
        self::assertThat($john->getFirstName(), self::equalTo('John'));
        self::assertThat($john->getLastName(), self::equalTo('Doe'));
    }

    /**
     * @test
     * @testdox instanciation de la classe à partir de son alias.
     */
    public function shouldInstantiateClassFromClassAlias()
    {
        $john = new AliasPerson('John', 'Doe');
        self::assertThat(get_class($john), self::equalTo('koan\base\ns\KoanPersonToUseWithAlias')); // This assertion is for learning purpose only. Use Clazz::class instead.
        self::assertThat($john->getFirstName(), self::equalTo('John'));
        self::assertThat($john->getLastName(), self::equalTo('Doe'));
    }

    /**
     * @test
     * @testdox instanciation d'une classe de l'espace de noms global.
     */
    public function shouldInstantiateClassFromGlobalNamespace()
    {
        $date = new DateTime("2019-09-01T12:00:00");
        self::assertThat($date, self::isInstanceOf('DateTime')); // This assertion is for learning purpose only. Use Clazz::class instead.
    }
}