<?php


namespace koan\classes;


use PHPUnit\Framework\TestCase;

class AbstractClassTest extends TestCase
{
    /**
     * @test
     * @testdox la classe concrète doit implémenter toutes les méthodes abstraites de la classe abstraite dont elle hérite.
     */
    public function shouldImplementAbstractClass()
    {
        //FIXME: create the Developer class
        $developer = new Developer('Jane', 'Doe');

        self::assertThat($developer, self::isInstanceOf(AbstractPerson::class));
        self::assertThat($developer->getOccupation(), self::equalTo('Developer'));
    }
}