<?php


namespace koan\classes;

if (!defined('___')) define('___', null);

use PHPUnit\Framework\TestCase;

class MagicMethodsTest extends TestCase
{
    /**
     * @test
     * @testdox constructeur de la classe
     */
    public function shouldCreateAnInstance()
    {
        $transaction = new Transaction(new \DateTime('2019-09-20 14:00:00'), Transaction::TYPE_EXPENSE, 24.50, 'Le restaurant de la gare', 'Current expenses');

        self::assertThat($transaction, self::isInstanceOf(Transaction::class));
        self::assertThat(___, self::equalTo(new \DateTime('2019-09-20 14:00:00')));
        self::assertThat(___, self::equalTo(Transaction::TYPE_EXPENSE));
        self::assertThat(___, self::equalTo(24.50));
        self::assertThat(___, self::equalTo('Le restaurant de la gare'));
        self::assertThat(___, self::equalTo('Current expenses'));
        self::assertThat(___, self::isNull());
    }

    /**
     * @test
     * @testdox affiche l'objet sous forme d'une chaine de caractères
     *
     * @see https://www.php.net/manual/fr/language.oop5.magic.php
     */
    public function shouldRepresentsObjectAsString()
    {
        $transaction = new Transaction(new \DateTime('2019-09-20 14:00:00'), Transaction::TYPE_EXPENSE, 24.50, 'Le restaurant de la gare', 'Current expenses', 'eating');

        self::assertThat(___, self::equalTo('Date: 2019-09-20 14:00:00, Type: expense, Amount: 24.50, Payee: Le restaurant de la gare, Account: Current expenses, Category: eating'));
    }
}