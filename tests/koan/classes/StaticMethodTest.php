<?php


namespace koan\classes;

if (!defined('___')) define('___', null);

use PHPUnit\Framework\TestCase;

class StaticMethodTest extends TestCase
{
    /**
     * @test
     * @testdox appel d'un attribut sans avoir à l'instancier la classe
     */
    public function shouldCallAttributeWithoutInstanciateClassBefore()
    {
        self::assertThat(Operator::$pi, self::equalTo(3.14));
    }

    /**
     * @test
     * @testdox appel de la méthode sans avoir l'instancier la classe
     */
    public function shouldCallMethodWithoutInstanciateClassBefore()
    {
        self::assertThat(Operator::add(2, 3), self::equalTo(5));
    }

    /**
     * @test
     * @testdox utilisation d'une constante de classe pour charger l'état de l'instance de Account.
     */
    public function classCouldDefineConstant()
    {
        $account = new Account('Current account');

        $account->enable();

        self::assertThat($account->getState(), self::equalTo(Account::ENABLED));
    }
}
