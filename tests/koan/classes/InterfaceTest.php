<?php


namespace koan\classes;


use PHPUnit\Framework\TestCase;

class InterfaceTest extends TestCase
{
    private $cars;
    /**
     * @before
     */
    public function init()
    {
        $this->cars[] = new Car('Renault', 'Zoé', 10000);
        $this->cars[] = new Car('Peugeot', '508', 20000);
    }

    /**
     * @test
     * @testdox la classe 'CarDealership' implémente les méthodes de l'interface 'Dealership'.
     */
    public function shouldImplementInterface()
    {
        $carDealership = new CarDealership($this->cars);

        self::assertThat($carDealership, self::isInstanceOf(Dealership::class));
        self::assertThat($carDealership, self::isInstanceOf(CarDealership::class));
        self::assertThat($carDealership->fetchAll(), self::equalTo($this->cars));
    }

    /**
     * @test
     * @testdox la classe 'CarDealership' implémente plusieurs interfaces.
     */
    public function shouldImplementSeveralInterfaces()
    {
        $carDealership = new CarDealership($this->cars);

        self::assertThat($carDealership, self::isInstanceOf(Dealership::class));
        self::assertThat($carDealership, self::isInstanceOf(CarDealership::class));
        self::assertThat($carDealership, self::isInstanceOf(\Countable::class));
        self::assertThat($carDealership->fetchAll(), self::equalTo($this->cars));
        self::assertThat($carDealership->count(), self::equalTo(2));
    }
}