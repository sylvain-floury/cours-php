<?php

namespace koan\classes;

use PHPUnit\Framework\TestCase;

if (!defined('___')) define('___', null);

class ClassTest  extends TestCase
{
    /**
     * @test
     * @testdox mot-clé qui permet d'instancier une classe.
     */
    public function shouldInstantiateClass()
    {
        self::assertThat(___, self::isInstanceOf(MyClass::class));
    }

    /**
     * @test
     * @testdox initialisation des paramètres d'une instance grâce au constructeur.
     */
    public function shouldInitializeParametersWhenInstantiateClass()
    {
        $person = ___;
        self::assertInstanceOf(Person::class, $person);
        self::assertThat($person->firstName, self::equalTo('John'));
        self::assertThat($person->lastName, self::equalTo('Doe'));
    }

    /**
     * @test
     * @testdox chaque instance a ses propres valeurs pour les attributs
     */
    public function shouldHaveDifferentsParametersValues()
    {
        $jane = new Person('Jane', 'Doe');
        $vicky = new Person('Vicky', 'Doe');
        self::assertThat($jane->firstName, self::equalTo(___));
        self::assertThat($vicky->firstName, self::equalTo(___));
    }

    /**
     * @test
     * @testdox le comportement des méthodes est identique pour toutes les instances d'une même classe.
     */
    public function shouldGreetIdenticallyWithSpecificFirstName()
    {
        $john = new Person('John', 'Doe');
        $jane = new Person('Jane', 'Doe');

        self::assertThat($john->greeting(), self::matches('___'));
        self::assertThat($jane->greeting(), self::matches('___'));
    }

    /**
     * @test
     * @testdox la classe Student::class est une sous classe de Person.
     */
    public function shouldInheriteFromSuperClass()
    {
        $student = new Student('Paul', 'Smith');

        self::assertThat($student, self::isInstanceOf(Student::class));
        self::assertThat($student, self::isInstanceOf(Person::class));
    }

    /**
     * @test
     * testdox les propriétés "private" ne sont pas accessibles directement.
     */
    public function shouldAccessToParentAttribute()
    {
        $person = new PersonEncapsulated('John', 'Doe');

        self::assertThat(___, self::equalTo('John'));
        self::assertThat(___, self::equalTo('Doe'));
    }

    /**
     * @test
     * @testdox les attributs 'firstName' et 'lastName' sont accessibles et modifiables directement.
     */
    public function shouldUpdateFirstName()
    {
        $personalDetails = new PersonalDetails('Jane', 'Doe');

        $personalDetails->firstName = 'John';

        self::assertThat($personalDetails->firstName, self::equalTo('John'));
        self::assertThat($personalDetails->lastName, self::equalTo('Doe'));
    }

    /**
     * @test
     * @testdox les méthodes des sous-classes peuvent modifier les attributs protégés de leur super classe
     */
    public function subClassCouldUpdateParentClassAttribute()
    {
        $electricCar = new ElectricCar('Renault', 'Zoé', 10000);

        $electricCar->resetMileage();

        self::assertThat($electricCar->getMileage(), self::equalTo(0));

    }

    /**
     * @test
     * @testdox seules les méthodes de la classe peuvent accéder aux attributs 'private'.
     */
    public function onlyClassMethodCouldChangePrivateAttribute()
    {
        $electricCar = new ElectricCar('Renault', 'Zoo', 10000);

        $electricCar->setType('Zoé');

        self::assertThat($electricCar->getType(), self::equalTo('Zoé'));
    }

    /**
     * @test
     * @testdox la méthode de la sous classe complète celle de la super classe à l'aide de la redéfinition.
     */
    public function shouldOverrideParentMethod()
    {
        $student = new StudentOverride('John', 'Doe');
        self::assertThat($student->greeting(), self::matches('Hello John! Have a good day.'));
    }
}
