<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class DeveloperTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnFirstname()
    {
        $someone = new Developer('John', 'Doe');

        $this->assertEquals('John', $someone->getFirstName());
    }
}
