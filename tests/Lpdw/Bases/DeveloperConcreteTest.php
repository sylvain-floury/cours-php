<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class DeveloperConcreteTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnProfession()
    {
        $someone = new DeveloperConcrete('John', 'Doe');

        $this->assertEquals('developer', $someone->getOccupation());
    }
}
