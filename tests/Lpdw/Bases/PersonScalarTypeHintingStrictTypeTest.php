<?php
declare(strict_types = 1);

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class PersonScalarTypeHintingStrictTypeTest extends TestCase
{
    /**
     * @test
     */
    public function shouldRejectParameter()
    {
        $person = new PersonScalarTypeHinting();
        $this->expectException(\TypeError::class);
        $person->setFirstName(4);
    }
}
