<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class PersonReturnNullableTypeHintingTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnErrorOnNull()
    {
        $person = new PersonReturnTypeHinting();
        $this->expectException(\TypeError::class);
        $person->setFirstName(null);
    }

    /**
     * @test
     */
    public function shouldReturnNull()
    {
        $person = new PersonReturnNullableTypeHinting();
        $person->setFirstName(null);
        self::assertNull($person->getFirstName());
    }
}
