<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class PersonEncapsulatedTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnPersonWithDifferentFirstname()
    {
        $someone = new PersonEncapsulated('John', 'Doe');
        $someoneElse = new PersonEncapsulated('Jane', 'Doe');

        $this->expectException(\Error::class);
        $this->assertEquals('John', $someone->firstName);
        $this->assertEquals('Jane', $someoneElse->firstName);
    }
}
