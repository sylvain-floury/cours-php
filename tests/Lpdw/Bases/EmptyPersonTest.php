<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class EmptyPersonTest extends TestCase
{
    /**
     * @test
     */
    public function shouldInstantiateClass()
    {
        $someone = new EmptyPerson();
        self::assertInstanceOf(EmptyPerson::class, $someone);
    }
}
