<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class PersonScalarTypeHintingTest extends TestCase
{
    /**
     * @test
     */
    public function shouldRejectParameter()
    {
        $person = new PersonScalarTypeHinting();
        $person->setFirstName(4);
        Assert::assertEquals("4", $person->getFirstName());
    }
}
