<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class PersonReturnTypeHintingTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnString()
    {
        $person = new PersonReturnTypeHinting();
        $person->setFirstName("John");
        Assert::assertEquals("John", $person->getFirstName());
    }
}
