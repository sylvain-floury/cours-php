<?php

namespace Lpdw\Bases;

use PHPUnit\Framework\TestCase;

class ArrayPersonStorageCountableTest extends TestCase
{
    private $dummyPopulation;

    /**
     * @before
     */
    public function init()
    {
        $someone = new Person('John', 'Doe');
        $someoneElse = new Person('Jane', 'Doe');

        $this->dummyPopulation[] = $someone;
        $this->dummyPopulation[] = $someoneElse;
    }

    /**
     * @test
     */
    public function shouldCountAllPersons()
    {
        $personStorage = new ArrayPersonStorageCountable($this->dummyPopulation);

        $this->assertCount(2, $personStorage);
    }
}
