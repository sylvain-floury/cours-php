<?php

namespace Lpdw\Platform;

use PHPUnit\Framework\TestCase;

class StringUtilsTest extends TestCase
{
    /**
     * @test
     */
    public function shouldCapitalizeString() {
        $this->assertThat(StringUtils::capitalize('test'), self::equalTo('TEST'));
    }
}