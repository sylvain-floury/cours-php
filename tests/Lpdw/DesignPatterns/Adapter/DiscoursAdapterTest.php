<?php

namespace Lpdw\DesignPatterns\Adapter;

use PHPUnit\Framework\TestCase;

class DiscoursAdapterTest extends TestCase
{
    /**
     * @test
     */
    public function shouldAdapteDiscours()
    {
        $speakerApplication = new SpeakerApplication();

        $discours = new PhpDiscours("Présentation de PHP");

        $discoursAdapter = new DiscoursAdapterSolution($discours);

        $this->assertEquals("Présentation de PHP", $speakerApplication->talk($discoursAdapter));
    }
}
