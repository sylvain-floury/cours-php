<?php

namespace Lpdw\DesignPatterns\Observer;

use Lpdw\DesignPatterns\Observer\Service\TemperatureDisplay;
use PHPUnit\Framework\TestCase;

class ThermometerSolutionTest extends TestCase
{
    /**
     * @test
     */
    public function should_notify_subject()
    {
        $subject = new ThermometerSolution();
        $observer = $this->createMock(TemperatureDisplay::class);
        $observer->expects(self::once())
            ->method('update');
        $subject->attach($observer);

        $subject->notify();
    }

    /**
     * @test
     */
    public function should_not_notify_detached_subject()
    {
        $subject = new ThermometerSolution();
        $observer = $this->createMock(TemperatureDisplay::class);
        $observer->expects(self::never())
            ->method('update');
        $subject->attach($observer);
        $subject->detach($observer);

        $subject->notify();
    }

}
