<?php

namespace Lpdw\DesignPatterns\Observer\Service;

use Lpdw\DesignPatterns\Observer\Thermometer;
use Lpdw\DesignPatterns\Observer\ThermometerSolution;
use PHPUnit\Framework\TestCase;

class TemperatureDisplaySolutionTest extends TestCase
{
    private $thermometer;
    private $temperatureDisplay;

    /**
     * @before
     */
    public function init()
    {
        $this->thermometer  = new ThermometerSolution();
        $this->temperatureDisplay = new TemperatureDisplaySolution();
        $this->thermometer->attach($this->temperatureDisplay);
    }
    /**
     * @test
     */
    public function shouldBeNotify()
    {
        $this->thermometer->setTemperature(20.5);
        $this->thermometer->notify();
        $this->assertEquals(20.5, $this->temperatureDisplay->getAmbientTemperature());

        $this->thermometer->setTemperature(22.5);
        $this->thermometer->notify();
        $this->assertEquals(22.5, $this->temperatureDisplay->getAmbientTemperature());
    }
}
