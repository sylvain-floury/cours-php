<?php

namespace Lpdw\DesignPatterns\Factory;

use PHPUnit\Framework\TestCase;

class CarColorFactoryTest extends TestCase
{
    private static $factory;

    /**
     * @before
     */
    public function init()
    {
        self::$factory = new CarFactory();
    }

    /**
     * @test
     */
    public function shouldCreateRedCar()
    {
        $car = self::$factory->createWithColor("red");
        $this->assertThat($car->countWheels(), self::equalTo(4));
        $this->assertEquals($car->getColor(), self::equalTo("red"));
    }
}
