<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Service;

use Lpdw\DesignPatterns\DependencyInjection\Model\Plane;
use PHPUnit\Framework\TestCase;
use Pimple\Container;

class TravelDicServiceTest extends TestCase
{
    private $container;

    /**
     * @before
     */
    public function setupContainer()
    {
        $this->container = new Container(); // <1>

        $this->container['vehicle'] = function ($c) { // <2>
            return new Plane();
        };

        $this->container['travel_service'] = function ($c) { // <3>
            return new TravelDiService($c['vehicle']);
        };
    }

    /**
     * @test
     */
    public function shouldtravelTo()
    {
        self::assertThat($this->container['travel_service']->travelTo('Paris'), self::equalTo('l\'avion a atteri à Paris')); // <4>
    }
}
