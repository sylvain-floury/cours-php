<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Service;

use Lpdw\DesignPatterns\DependencyInjection\Model\Car;
use Lpdw\DesignPatterns\DependencyInjection\Model\Plane;
use PHPUnit\Framework\TestCase;

class TravelDiServiceTest extends TestCase
{
    /**
     * @test
     */
    public function shouldTravelByPlaneTo()
    {
        $travelService = new TravelDiService(new Plane());
        self::assertThat($travelService->travelTo('Paris'), self::equalTo('l\'avion a atteri à Paris'));
    }

    /**
     * @test
     */
    public function shouldTravelByCarTo()
    {
        $travelService = new TravelDiService(new Car());
        self::assertThat($travelService->travelTo('Paris'), self::equalTo('la voiture est arrivée à Paris'));
    }
}
