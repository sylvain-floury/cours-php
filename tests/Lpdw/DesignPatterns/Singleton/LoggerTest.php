<?php

namespace Lpdw\DesignPatterns\Singleton;

use PHPUnit\Framework\TestCase;

class LoggerTest extends TestCase
{
    /**
     * @test
     */
    public function shouldGetInstance()
    {
        $logger = LoggerSolution::getInstance();
        $this->assertInstanceOf(LoggerSolution::class, $logger);
    }

    /**
     * @test
     */
    public function shouldNotWorkWithNew()
    {
        try {
            $logger = new LoggerSolution();
            $this->assertInstanceOf(LoggerSolution::class, $logger);
        } catch (\Throwable $e) {
            self::assertEquals('Call to private Lpdw\DesignPatterns\Singleton\Logger::__construct() from context \'Lpdw\DesignPatterns\Singleton\LoggerTest\'', $e->getMessage());
        }
    }
}
