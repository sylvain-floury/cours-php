<?php

namespace Lpdw\DesignPatterns\Singleton;

use PHPUnit\Framework\TestCase;

class LoggerSolutionTest extends TestCase
{
    /**
     * @test
     */
    public function shouldGetInstance()
    {
        $logger = LoggerSolution::getInstance();
        $this->assertInstanceOf(LoggerSolution::class, $logger);
    }

    /**
     * @test
     */
    public function shouldNotWorkWithNew()
    {
        try {
            $logger = new LoggerSolution();
            $this->assertInstanceOf(LoggerSolution::class, $logger);
        } catch (\Throwable $e) {
            self::assertEquals('Call to private Lpdw\DesignPatterns\Singleton\LoggerSolution::__construct() from context \'Lpdw\DesignPatterns\Singleton\LoggerSolutionTest\'', $e->getMessage());
        }
    }
}
