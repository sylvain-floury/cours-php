<?php

namespace Lpdw\DesignPatterns\Iterator\Collection;

use Iterator;
use Lpdw\DesignPatterns\Iterator\Model\Person;
use PHPUnit\Framework\TestCase;

class PersonCollectionSolutionTest extends TestCase
{
    private Iterator $personCollection;
    private Person $person1;
    private Person $person2;

    /**
     * @before
     */
    public function init()
    {
        $this->person1 = new Person('John', 'Doe');
        $this->person2 = new Person('Jane', 'Doe');

        $this->dummyPopulation[] = $this->person1;
        $this->dummyPopulation[] = $this->person2;

        $this->personCollection = new PersonCollectionSolution($this->dummyPopulation);
    }

    /**
     * @test
     */
    public function should_return_current_person()
    {
        self::assertThat($this->personCollection->current(), self::equalTo($this->person1));
    }

    /**
     * @test
     */
    public function should_return_next_person()
    {
        $this->personCollection->next();
        self::assertThat($this->personCollection->current(), self::equalTo($this->person2));
    }

    /**
     * @test
     */
    public function should_return_zero_for_first_person()
    {
        self::assertThat($this->personCollection->key(), self::equalTo(0));
    }

    /**
     * @test
     */
    public function should_return_one_for_second_person()
    {
        $this->personCollection->next();
        self::assertThat($this->personCollection->key(), self::equalTo(1));
    }

    /**
     * @test
     */
    public function should_return_true_if_collection_hasn_t_reached_last_person()
    {
        self::assertThat($this->personCollection->valid(), self::isTrue());
    }

    /**
     * @test
     */
    public function should_return_false_if_collection_has_reached_last_person()
    {
        $this->personCollection->next();
        $this->personCollection->next();
        self::assertThat($this->personCollection->valid(), self::isFalse());
    }

    /**
     * @test
     */
    public function should_reset_collection()
    {
        $this->personCollection->next();
        $this->personCollection->rewind();

        self::assertThat($this->personCollection->current(), self::equalTo($this->person1));
    }

    /**
     * @test
     */
    public function shouldIterateCollection()
    {

        self::assertTrue($this->personCollection->valid());
        $test = $this->personCollection->current();
        $this->assertEquals('John', $test->getFirstName());
        $this->personCollection->next();
        self::assertTrue($this->personCollection->valid());
        $this->assertEquals('Jane', $this->personCollection->current()->getFirstName());
        $this->personCollection->next();
        self::assertFalse($this->personCollection->valid());
    }

    /**
     * @test
     */
    public function shouldIterateThroughCollection()
    {
        $this->personCollection = new PersonCollectionSolution($this->dummyPopulation);

        $nbPersons = 0;
        foreach ($this->personCollection as $person) {
            $this->assertInstanceOf(Person::class, $person);
            $nbPersons ++;
        }

        $this->assertEquals(2, $nbPersons);
    }
}
