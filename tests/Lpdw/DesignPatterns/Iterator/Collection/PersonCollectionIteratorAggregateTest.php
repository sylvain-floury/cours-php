<?php

namespace Lpdw\DesignPatterns\Iterator\Collection;

use PHPUnit\Framework\TestCase;
use Lpdw\DesignPatterns\Iterator\Model\Person;

class PersonCollectionIteratorAggregateTest extends TestCase
{
    private $dummyPopulation;

    /**
     * @before
     */
    public function init()
    {
        $someone = new Person('John', 'Doe');
        $someoneElse = new Person('Jane', 'Doe');

        $this->dummyPopulation[] = $someone;
        $this->dummyPopulation[] = $someoneElse;
    }

    /**
     * @test
     */
    public function shouldIterateCollection()
    {
        $personCollection = new PersonCollectionIteratorAggregate($this->dummyPopulation);
        $personCollectionIterator = $personCollection->getIterator();

        self::assertTrue($personCollectionIterator->valid());
        $test = $personCollectionIterator->current();
        $this->assertEquals('John', $test->getFirstname());
        $personCollectionIterator->next();
        self::assertTrue($personCollectionIterator->valid());
        $this->assertEquals('Jane', $personCollectionIterator->current()->getFirstname());
        $personCollectionIterator->next();
        self::assertFalse($personCollectionIterator->valid());
    }

    /**
     * @test
     */
    public function shouldIterateThroughtCollection()
    {
        $personCollection = new PersonCollectionIteratorAggregate($this->dummyPopulation);

        $nbPersons = 0;
        foreach ($personCollection as $person) {
            $this->assertInstanceOf(Person::class, $person);
            $nbPersons ++;
        }

        $this->assertEquals(2, $nbPersons);
    }
}
