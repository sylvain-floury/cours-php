<?php

namespace Lpdw\Oop;

use Exception;
use PHPUnit\Framework\TestCase;

class ArrayPersonStorageBoundedTest extends TestCase
{
    /**
     * @before
     */
    public function init()
    {
        $someone = new Developer('John', 'Doe');
        $someoneElse = new Developer('Jane', 'Doe');

        $this->dummyPopulation[] = $someone;
        $this->dummyPopulation[] = $someoneElse;
    }

    /**
     * @test
     */
    public function shouldNotGetDeveloper()
    {
        $personStorage = new ArrayPersonStorageBounded($this->dummyPopulation);

        $this->expectException(Exception::class);
        $personStorage->get(2);

    }
}
