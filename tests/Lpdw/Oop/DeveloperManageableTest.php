<?php

namespace Lpdw\Oop;

use PHPUnit\Framework\TestCase;

class DeveloperManageableTest extends TestCase
{
    /**
     * @test
     */
    public function shouldHaveAManager()
    {
        $developer = new DeveloperManageable('John', 'Doe');
        $manager = new Manager('Jane', 'Doe');

        $developer->setManager($manager);

        $this->assertEquals('Jane Doe', $developer->getManager()->__toString());
    }
}
