<?php

namespace Lpdw\Oop;

use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase
{
    /**
     * @test
     */
    public function shouldBeAtDraftState()
    {
        $article = new Article();
        $article->setState(Article::STATE_DRAFT);
        self::assertNotEquals(Article::STATE_PUBLISHED, $article->getState());
        self::assertNotEquals(Article::STATE_ARCHIVED, $article->getState());
    }
}
