<?php

namespace Lpdw\Oop;

use PHPUnit\Framework\TestCase;

class ArrayPersonStorageTest extends TestCase
{
    /**
     * @before
     */
    public function init()
    {
        $someone = new Developer('John', 'Doe');
        $someoneElse = new Developer('Jane', 'Doe');

        $this->dummyPopulation[] = $someone;
        $this->dummyPopulation[] = $someoneElse;
    }

    /**
     * @test
     */
    public function shouldAddPersonEncapsulated()
    {
        $personStorage = new ArrayPersonStorage([]);
        $someone = new Developer('John', 'Nobody');

        $personStorage->add($someone);
        $this->assertCount(1, $personStorage);
    }

    /**
     * @test
     */
    public function shouldNotAddPerson()
    {
        $personStorage = new ArrayPersonStorage([]);
        $someone = new Person('John', 'Nobody');

        $this->expectException(\TypeError::class);
        $personStorage->add($someone);
    }

    /**
     * @test
     */
    public function shouldAddDeveloper()
    {
        $personStorage = new ArrayPersonStorage([]);
        $someone = new Developer('John', 'Nobody');

        $personStorage->add($someone);
        $this->assertCount(1, $personStorage);
    }

    /**
     * @test
     */
    public function shouldGetDeveloper()
    {
        $personStorage = new ArrayPersonStorage($this->dummyPopulation);
        $someone = $personStorage->get(0);

        $this->assertInstanceOf(AbstractPerson::class, $someone);
    }

    /**
     * @test
     */
    public function shouldNotGetDeveloper()
    {
        $personStorage = new ArrayPersonStorage($this->dummyPopulation);
        try {
            $someone = $personStorage->get(2);
            $this->assertInstanceOf(AbstractPerson::class, $someone);
        } catch (\Throwable $e) {
            self::assertEquals('Undefined offset: 2', $e->getMessage());
        }
    }
}
