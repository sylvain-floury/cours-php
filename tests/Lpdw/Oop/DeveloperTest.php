<?php

namespace Lpdw\Oop;

use PHPUnit\Framework\TestCase;

class DeveloperTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnAsString()
    {
        $someone = new Developer('John', 'Doe');

        $this->assertEquals('John Doe', $someone->__toString());
    }
}
