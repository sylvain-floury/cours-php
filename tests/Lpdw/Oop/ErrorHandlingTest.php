<?php

namespace Lpdw\Oop;

use PHPUnit\Framework\TestCase;

class ErrorHandlingTest extends TestCase
{
    /**
     * @test
     */
    public function shouldRiseTypeError()
    {
        try {
            $personStorage = new ArrayPersonStorage([]);
            $personStorage->add("John Doe");
        } catch (\Error $e) {
            self::assertStringContainsString('Argument 1 passed to Lpdw\Oop\ArrayPersonStorage::add() must be an instance of Lpdw\Oop\AbstractPerson, string given', $e->getMessage());
        }
    }

    /**
     * @test
     */
    public function shouldRiseDivideByZeroError()
    {
        try {
            $result = 10/0;
        } catch (\Throwable $e) {
            self::assertStringContainsString('Division by zero', $e->getMessage());
        }
    }
}
