<?php


namespace kata;


use PHPUnit\Framework\TestCase;

class LeapYearTest extends TestCase
{
    /**
     * @test
     */
    public function should_be_true_when_years_is_divisible_by_4()
    {
        self::assertThat(LeapYear::isLeap(1996), self::isTrue());
    }

    /**
     * @test
     */
    public function should_be_false_when_year_is_not_divisibleB_by_4()
    {
        self::assertThat(LeapYear::isLeap(1997), self::isFalse());
    }

    /**
     * @test
     */
    public function should_be_false_when_years_is_divisible_by_4_and_100()
    {
        self::assertThat(LeapYear::isLeap(1900), self::isFalse());
    }

    /**
     * @test
     */
    public function should_be_true_when_years_is_divisible_by_4_and_100_and_400()
    {
        self::assertThat(LeapYear::isLeap(2000), self::isTrue());
    }
}
