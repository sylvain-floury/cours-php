<?php

namespace kata;

use PHPUnit\Framework\TestCase;

/**
 * There are several solutions to validate these tests,
 * here are some resources that will help you.
 *
 * https://www.php.net/manual/fr/function.preg-match.php
 * https://regex101.com/
 * https://www.php.net/manual/fr/book.ctype.php
 */

class CheckPasswordTest extends TestCase
{
    /**
     * @test
     */
    public function empty_password_should_be_refused()
    {
        self::assertFalse(CheckPassword::isValid(''));
    }

    /**
     * @test
     */
    public function password_conformed_to_policies_should_be_validated()
    {
        self::assertTrue(CheckPassword::isValid('AZ3rt-uIoP'));
    }

    /**
     * @test
     */
    public function password_should_have_at_least_8_characters()
    {
        self::assertFalse(CheckPassword::isValid('AZ3rt-u'));
    }

    /**
     * @test
     */
    public function password_should_have_at_least_1_digit()
    {
        self::assertFalse(CheckPassword::isValid('AZErt-uIoP'));
    }

    /**
     * @test
     */
    public function password_should_have_at_least_1_uppercase_character()
    {
        self::assertFalse(CheckPassword::isValid('az3rt-uiop'));
    }

    /**
     * @test
     */
    public function password_should_have_at_least_1_lowercase_character()
    {
        self::assertFalse(CheckPassword::isValid('AZ3RT-UIOP'));
    }

    /**
     * @test
     */
    public function password_should_have_at_least_1_special_character()
    {
        self::assertFalse(CheckPassword::isValid('AZ3RTyUiOp'));
    }
}
