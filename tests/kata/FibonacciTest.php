<?php


namespace kata;


use PHPUnit\Framework\TestCase;

class FibonacciTest extends TestCase
{
    /**
     * @test
     */
    public function f1_is_1()
    {
        self::assertThat(Fibonacci::compute(1), self::equalTo(1));
    }

    /**
     * @test
     */
    public function f2_is_1()
    {
        self::assertThat(Fibonacci::compute(1), self::equalTo(1));
    }

    /**
     * @test
     */
    public function f3_is_2()
    {
        self::assertThat(Fibonacci::compute(3), self::equalTo(2));
    }

    /**
     * @test
     */
    public function f4_is_3()
    {
        self::assertThat(Fibonacci::compute(4), self::equalTo(3));
    }

    /**
     * @test
     */
    public function f8_is_21()
    {
        self::assertThat(Fibonacci::compute(8), self::equalTo(21));
    }

    /**
     * @test
     */
    public function f35_is_9227465()
    {
        self::assertThat(Fibonacci::compute(35), self::equalTo(9227465));
    }

    /**
     * @test
     */
    public function f_memo_35_is_9227465()
    {
        self::assertThat(Fibonacci::computeMemo(35), self::equalTo(9227465));
    }
}
