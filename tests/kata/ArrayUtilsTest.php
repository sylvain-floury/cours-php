<?php


namespace kata;


use PHPUnit\Framework\TestCase;

class ArrayUtilsTest extends TestCase
{
    /**
     * @test
     */
    public function element_is_in_array()
    {
       self::assertThat(ArrayUtils::contains(['a', 'b', 'c', 'd'], "a"), self::isTrue());
    }

    /**
     * @test
     */
    public function element_is_not_in_array()
    {
        self::assertThat(ArrayUtils::contains(['a', 'b', 'c', 'd'], "f"), self::isFalse());
    }

    /**
     * @test
     */
    public function integer_is_in_array()
    {
        self::assertThat(ArrayUtils::contains([1, 2, 3, 4], 1), self::isTrue());
    }

    /**
     * @test
     */
    public function element_with_appopriate_type_is_not_in_array()
    {
        self::assertThat(ArrayUtils::contains([1,2,3,4], "1"), self::isFalse());
    }
}
