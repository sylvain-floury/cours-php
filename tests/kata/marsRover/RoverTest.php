<?php

namespace kata\marsRover;

use PHPUnit\Framework\TestCase;

class RoverTest extends TestCase
{
    /**
     * @test
     */
    public function should_validate_command_10_10_2_1_S_M()
    {
        $rover = new Rover('10 10 2 1 S M');

        self::assertThat($rover->validateCommand(), self::isTrue());
    }

    /**
     * @test
     */
//    public function should_validate_command_with_grid_size_initial_position_and_without_movement()
//    {
//        $rover = new Rover('10 10 2 1 S');
//
//        self::assertThat($rover->validateCommand(), self::isTrue());
//    }

    /**
     * @test
     */
//    public function should_invalidate_command_10_10_2_1()
//    {
//        $rover = new Rover('10 10 2 1');
//
//        self::assertThat($rover->validateCommand(), self::isFalse());
//    }

    /**
     * @test
     */
    public function should_return_grid_10_10_for_command_10_10_2_1_S_M()
    {
        $rover = new Rover('10 10 2 1 S M');

        self::assertThat($rover->grid, self::equalTo(new Grid(10, 10)));
    }

    /**
     * @test
     */
    public function should_return_rover_abscissa_ordinate_and_direction_from_command()
    {
        $rover = new Rover('10 10 2 1 S M');

        self::assertThat($rover->abscissa, self::equalTo(2));
        self::assertThat($rover->ordinate, self::equalTo(1));
        self::assertThat($rover->direction, self::equalTo('S'));
    }

    /**
     * @test
     */
    public function should_return_rover_movements_from_command()
    {
        $rover = new Rover('10 10 2 1 S LMMRM');

        self::assertThat($rover->movements, self::equalTo(['L', 'M', 'M', 'R', 'M']));
    }

    /**
     * @test
     */
    public function should_broadcast_east_direction_when_rotate_left_from_south_direction()
    {
        $rover = new Rover('10 10 2 1 S L');

        $rover->move('L');

        self::assertThat($rover->abscissa, self::equalTo(2));
        self::assertThat($rover->ordinate, self::equalTo(1));
        self::assertThat($rover->direction, self::equalTo('E'));
    }

    /**
     * @test
     */
    public function should_broadcast_west_direction_when_rotate_left_from_north_direction()
    {
        $rover = new Rover('10 10 2 1 N L');

        $rover->move('L');

        self::assertThat($rover->abscissa, self::equalTo(2));
        self::assertThat($rover->ordinate, self::equalTo(1));
        self::assertThat($rover->direction, self::equalTo('W'));
    }

    /**
     * @test
     */
    public function should_broadcast_west_direction_when_rotate_right_from_south_direction()
    {
        $rover = new Rover('10 10 2 1 S R');

        $rover->move('R');

        self::assertThat($rover->abscissa, self::equalTo(2));
        self::assertThat($rover->ordinate, self::equalTo(1));
        self::assertThat($rover->direction, self::equalTo('W'));
    }

    /**
     * @test
     */
    public function should_broadcast_east_direction_when_rotate_right_from_north_direction()
    {
        $rover = new Rover('10 10 2 1 N R');

        $rover->move('R');

        self::assertThat($rover->abscissa, self::equalTo(2));
        self::assertThat($rover->ordinate, self::equalTo(1));
        self::assertThat($rover->direction, self::equalTo('E'));
    }

    /**
     * @test
     */
    public function should_broadcast_position_2_4_N_when_receive_command_5_5_4_3_N_LMMRM()
    {
        $rover = new Rover('5 5 4 3 N LMMRM');

        $position = $rover->broadcastPosition();

        self::assertThat($position, self::equalTo('2 4 N'));
    }
}
