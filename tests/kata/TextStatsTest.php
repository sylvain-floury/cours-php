<?php

namespace kata;

use PHPUnit\Framework\TestCase;

class TextStatsTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_count_2_words()
    {
        $text = "Hello world";
        self::assertThat(TextStats::countWords($text), self::equalTo(2));
    }

    /**
     * @test
     */
    public function it_should_count_words_usage()
    {
        $text = 'hello world';
        self::assertThat(TextStats::countWordsUsage($text), self::equalTo(['hello' => 1, 'world' => 1]));
    }

    /**
     * @test
     */
    public function it_should_count_word_usage_with_multiple_occurrences()
    {
        $text = 'he has one car and one house';
        self::assertThat(TextStats::countWordsUsage($text), self::equalTo(['he' => 1, 'has' => 1, 'one' => 2, 'car' => 1, 'and' => 1, 'house' => 1 ]));
    }

    /**
     * @test
     */
    public function it_should_count_word_usage_and_ignore_case()
    {
        $text = 'he has One car and ONE house';
        self::assertThat(TextStats::countWordsUsage($text), self::equalTo(['he' => 1, 'has' => 1, 'one' => 2, 'car' => 1, 'and' => 1, 'house' => 1]));
    }
}
