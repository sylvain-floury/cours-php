<?php
declare(strict_types=1);

namespace kata;


use PHPUnit\Framework\TestCase;

class NumberUtilsTest extends TestCase
{
    /**
     * @test
     */
    public function two_is_even()
    {
        self::assertThat(NumberUtils::isEven(2), self::isTrue());
    }

    /**
     * @test
     */
    public function one_is_not_even()
    {
        self::assertThat(NumberUtils::isEven(1), self::isFalse());
    }

    /**
     * @test
     */
    public function zero_is_not_even()
    {
        self::assertThat(NumberUtils::isEven(0), self::isFalse());
    }

    /**
     * @test
     */
    public function minus_four_is_even()
    {
        self::assertThat(NumberUtils::isEven(-4), self::isTrue());
    }

    /**
     * @test
     */
    public function zero_point_five_is_not_even()
    {
        self::assertThat(NumberUtils::isEven(0.5), self::isFalse());
    }
}
