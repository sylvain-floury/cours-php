<?php

namespace kata;


use PHPUnit\Framework\TestCase;

class SpeedConverterTest extends TestCase
{
    /**
     * @test
     */
    public function should_convert_10_8_km_per_hour_to_3_m_per_second()
    {
        self::assertThat(SpeedConverter::convert(10.8), self::equalTo(3));
    }

    /**
     * @test
     */
    public function should_convert_14_0_km_per_hour_to_3_m_per_second()
    {
        self::assertThat(SpeedConverter::convert(14.0), self::equalTo(3));
    }

    /**
     * @test
     */
    public function should_convert_0_km_per_hour_to_0_m_per_second()
    {
        self::assertThat(SpeedConverter::convert(0.0), self::equalTo(0));
    }
}
