#!/usr/bin/env bash

asciidocFile=$1

if [ -z "$asciidocFile" ]
then
  echo "Missing asciidoctor file"
  echo "Usage: build-static.sh doc/src/my-file.adoc"
  exit 1
fi

echo "Building $asciidocFile file"

docker run --rm --user="$(id -u):$(id -g)" \
-v $(pwd)/:/documents/ asciidoctor/docker-asciidoctor asciidoctor-revealjs \
-r asciidoctor-diagram -a revealjsdir=https://cdn.jsdelivr.net/npm/reveal.js@3.9.2 \
$asciidocFile \
-D doc/html-static/
