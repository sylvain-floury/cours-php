#!/usr/bin/env bash

revealPath=~/tmp/reveal.js/cours-php/
asciidocFile=$1

if [[ ! -f ~/tmp/reveal.js/cours-php/ ]]
then
  echo "Creating $revealPath"
  mkdir -p $revealPath
fi

if [ -z "$asciidocFile" ]
then
  echo "Missing asciidoctor file"
  echo "Usage: build-revealjs.sh doc/src/my-file.adoc"
  exit 1
fi

echo "Building $asciidocFile file"

docker run --rm --user="$(id -u):$(id -g)" \
-v $(pwd)/:/documents/ asciidoctor/docker-asciidoctor \
asciidoctor-revealjs -r asciidoctor-diagram \
-a revealjsdir=.. \
$asciidocFile \
-D doc/html/

echo "Copying HTML files in $revealPath"

cp -r doc/html/* $revealPath
