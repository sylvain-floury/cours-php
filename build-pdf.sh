#!/usr/bin/env bash

asciidocFile=$1

if [ -z "$asciidocFile" ]
then
  echo "Missing asciidoctor file"
  echo "Usage: build-pdf.sh doc/src/my-file.adoc"
  exit 1
fi

echo "Building $asciidocFile file"

docker run --rm --user="$(id -u):$(id -g)" \
-v $(pwd)/:/documents/ asciidoctor/docker-asciidoctor \
asciidoctor-pdf -r asciidoctor-diagram \
$asciidocFile \
-D doc/pdf/
