<?php


namespace koan\classes;


class Account
{
    const DISABLED = 0;
    const ENABLED = 1;

    private $name;
    private $state;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->state = self::DISABLED;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getState(): int
    {
        return $this->state;
    }

    // todo: add enable method
}