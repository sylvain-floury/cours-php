<?php


namespace koan\classes;


class Car
{
    private $brand;
    private $type;
    private $mileage;

    public function __construct(string $brand, string $type, int $mileage)
    {
        $this->brand = $brand;
        $this->type = $type;
        $this->mileage = $mileage;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getMileage(): int
    {
        return $this->mileage;
    }
}