<?php


namespace koan\classes;

class Transaction
{
    const TYPE_EXPENSE = 1;
    const TYPE_INCOME = 2;

    private $date;
    private $type;
    private $amount;
    private $payee;
    private $account;
    private $category;

    public function __construct(\DateTime $date, int $type, float $amount, string $payee, string $account, string $category = null)
    {
        $this->date = $date;
        $this->type = $type;
        $this->amount = $amount;
        $this->payee = $payee;
        $this->account = $account;
        $this->category = $category;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getPayee(): string
    {
        return $this->payee;
    }

    public function getAccount(): string
    {
        return $this->account;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @return string
     *
     * @see https://www.php.net/manual/fr/datetime.format.php
     * @see https://www.php.net/manual/fr/function.date.php
     */
    public function __toString()
    {
        return ''; //todo
    }
}