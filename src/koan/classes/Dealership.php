<?php


namespace koan\classes;


interface Dealership
{
    public function fetchAll():array;
}