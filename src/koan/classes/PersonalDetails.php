<?php


namespace koan\classes;


class PersonalDetails
{

    private $firstName; // should be accessible
    private $lastName;  // should be accessible

    public function __construct(string $firstName, string $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }
}