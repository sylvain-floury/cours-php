<?php


namespace koan\classes;


class ElectricCar extends Car
{
    public function resetMileage()
    {
        $this->mileage = 0;  // should be available
    }

    public function setType(string $type)  // is it the right place ?
    {
        $this->type = $type;
    }
}