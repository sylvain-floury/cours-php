<?php


namespace koan\base;


use Doctrine\Common\Collections\ArrayCollection;

class TransactionStore
{

    private $transactions;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function add(Transaction $transaction)
    {
        $this->transactions->add($transaction);
    }

    public function get(int $index)
    {
        if($index > $this->transactions->count() -1) {
            // todo: handle exception
        }

        return $this->transactions[$index];
    }

    public function exists($transaction): bool
    {
        return $this->transactions->exists(function ($k, $t) use ($transaction) {
            return $t == $transaction;
        });
    }
}