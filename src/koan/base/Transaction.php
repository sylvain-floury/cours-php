<?php


namespace koan\base;

//expense <> income | amount | payee | account | category |date
class Transaction
{
    private $date;
    private $payee;
    private $amount;

    public function __construct(\DateTime $date, string $payee, float $amount)
    {
        $this->date = $date;
        $this->payee = $payee;
        $this->amount = $amount;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function getPayee(): string
    {
        return $this->payee;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }


}