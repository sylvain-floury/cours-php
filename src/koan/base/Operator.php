<?php
declare(strict_types = 1);

namespace koan\base;


class Operator
{
    public static function divide(int $a, int $b)
    {
        return $a / $b;
    }
}
