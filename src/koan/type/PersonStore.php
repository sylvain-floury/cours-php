<?php


namespace koan\type;


use Doctrine\Common\Collections\ArrayCollection;

class PersonStore
{
    private $persons;

    public function __construct()
    {
        $this->persons = new ArrayCollection();
    }

    public function add(Person $person)
    {
        $this->persons->add($person);
    }

    public function count():int
    {
        return $this->persons->count();
    }

    public function first()
    {
        return $this->persons->first();
    }

    //FIXME: to complete
    public function addIfExists(Person $person) // should be corrected
    {
    }

    //FIXME: to complete
    public function last():Person
    {
    }
}
