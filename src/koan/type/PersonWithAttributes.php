<?php


namespace koan\type;


class PersonWithAttributes
{
    // Complete attributes definition
    public $firstName;
    public $lastName;
}
