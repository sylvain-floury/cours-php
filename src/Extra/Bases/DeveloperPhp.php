<?php

namespace Extra\Bases;

class DeveloperPhp extends Developer
{
    public function greeting()
    {
        return parent::greeting() . ' PHP';
    }
}
