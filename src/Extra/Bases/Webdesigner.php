<?php

namespace Extra\Bases;

class Webdesigner extends Person
{
    public function greeting()
    {
        return 'Hello ' . $this->firstName . ' Webdesigner !';
    }
}
