<?php

namespace Extra\Bases;

class Developer extends Person
{
    protected function greeting()
    {
        return parent::greeting() . ' I\'m a developper';
    }
}
