<?php

namespace Lpdw\Oop;

use Countable;

class ArrayPersonStorage implements PersonStorageInterface, Countable
{
    private $storage;

    public function __construct($persons)
    {
        $this->storage = $persons;
    }

    /**
     * @return AbstractPerson[]
     */
    public function fetchAll()
    {
        return $this->storage;
    }

    /**
     * @param AbstractPerson $person
     */
    public function add(AbstractPerson $person)
    {
        $this->storage[] = $person;
    }

    /**
     * @param int $index
     * @return AbstractPerson
     */
    public function get(int $index):?AbstractPerson
    {
        return $this->storage[$index];
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->storage);
    }
}
