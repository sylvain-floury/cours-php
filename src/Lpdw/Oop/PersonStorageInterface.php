<?php

namespace Lpdw\Oop;

interface PersonStorageInterface
{
    /**
     * @return AbstractPerson[]
     */
    public function fetchAll();

    /**
     * @param AbstractPerson $person
     */
    public function add(AbstractPerson $person);

    /**
     * @param int $index
     * @return AbstractPerson
     */
    public function get(int $index):?AbstractPerson;
}
