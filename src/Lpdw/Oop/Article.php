<?php

namespace Lpdw\Oop;

class Article
{
    private $title;
    private $text;
    private $state;

    const STATE_DRAFT = 1;
    const STATE_PUBLISHED = 2;
    const STATE_ARCHIVED = 3;

    /**
     * @return string
     */
    public function getTitle():string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText():string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getState():int
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState(int $state)
    {
        $this->state = $state;
    }
}
