<?php

namespace Lpdw\Oop;

use Countable;
use Exception;

class ArrayPersonStorageBounded implements PersonStorageInterface, Countable
{
    private $storage;

    public function __construct($persons)
    {
        $this->storage = $persons;
    }

    /**
     * @return AbstractPerson[]
     */
    public function fetchAll()
    {
        return $this->storage;
    }

    /**
     * @param AbstractPerson $person
     */
    public function add(AbstractPerson $person)
    {
        $this->storage[] = $person;
    }

    /**
     * @param int $index
     * @return AbstractPerson|null
     * @throws Exception
     */
    public function get(int $index):?AbstractPerson
    {
        if (!isset($this->storage[$index])) {
            throw new Exception('Le service ArrayPersonStorage ne dispose d\'aucune personne pour l\'index donné');
        }
        return $this->storage[$index];
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->storage);
    }
}
