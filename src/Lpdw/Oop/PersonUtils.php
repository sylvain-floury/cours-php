<?php

namespace Lpdw\Oop;

class PersonUtils
{
    public static $atttribute = 'value';

    public static function staticMethod()
    {
        return self::$atttribute;
    }
}
