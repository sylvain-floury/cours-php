<?php

namespace Lpdw\Oop;

abstract class AbstractPerson
{
    private $firstName;
    private $lastName;

    public function __construct($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function greeting()
    {
        return 'Hello ' . $this->firstName . '!';
    }

    public function __toString()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    abstract public function getProfession();
}
