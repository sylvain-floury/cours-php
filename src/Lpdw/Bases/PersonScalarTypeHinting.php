<?php

namespace Lpdw\Bases;

class PersonScalarTypeHinting
{
    private $firstName;
    private $lastName;

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }
}
