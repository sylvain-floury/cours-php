<?php

namespace Lpdw\Bases;

use Countable;

class ArrayPersonStorageTypeHinting implements PersonStorageTypeHintingInterface, Countable
{
    private $storage;

    /**
     * @return PersonEncapsulated[]
     */
    public function fetchAll()
    {
        return $this->storage;
    }

    /**
     * @param PersonEncapsulated $person
     */
    public function add(PersonEncapsulated $person)
    {
        $this->storage[] = $person;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->storage);
    }
}
