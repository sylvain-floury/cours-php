<?php

namespace Lpdw\Bases;

class Person
{
    public $firstName;
    public $lastName;

    public function __construct($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function greeting()
    {
        return 'Hello ' . $this->firstName . '!';
    }
}
