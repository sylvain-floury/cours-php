<?php

namespace Lpdw\Bases;

class DeveloperConcrete extends AbstractPerson
{
    public function greeting():string
    {
        return parent::greeting() . ' I\'m a developper';
    }

    public function getOccupation():string
    {
        return 'developer';
    }
}
