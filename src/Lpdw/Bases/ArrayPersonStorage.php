<?php

namespace Lpdw\Bases;

class ArrayPersonStorage implements PersonStorageInterface
{
    private $storage;

    public function __construct($persons)
    {
        $this->storage = $persons;
    }

    /**
     * @return Person[]
     */
    public function fetchAll()
    {
        return $this->storage;
    }
}
