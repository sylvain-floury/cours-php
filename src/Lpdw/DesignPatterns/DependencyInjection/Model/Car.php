<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Model;

class Car implements Vehicle
{
    public function movingTo(string $address):string
    {
        return 'la voiture est arrivée à ' . $address;
    }
}
