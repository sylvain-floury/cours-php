<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Service;

use Lpdw\DesignPatterns\DependencyInjection\Model\PlaneBasic;

class TravelBasicService
{
    public function travelTo(string $destination):string
    {
        $vehicle = new PlaneBasic(); // <1>
        return $vehicle->movingTo($destination);
    }
}
