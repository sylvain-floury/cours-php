<?php

namespace Lpdw\DesignPatterns\DependencyInjection\Service;

use Lpdw\DesignPatterns\DependencyInjection\Model\Vehicle;

class TravelDiService
{
    private $vehicle;

    public function __construct(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
    }

    public function travelTo(string $destination):string
    {
        return $this->vehicle->movingTo($destination);
    }
}
