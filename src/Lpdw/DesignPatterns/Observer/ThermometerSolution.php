<?php

namespace Lpdw\DesignPatterns\Observer;

use SplObjectStorage;
use SplObserver;

class ThermometerSolution implements \SplSubject
{
    private $temperature;

    private $observers;

    public function __construct()
    {
        $this->observers = new SplObjectStorage();
    }

    public function getTemperature():float
    {
        return $this->temperature;
    }

    public function setTemperature(float $temperature)
    {
        $this->temperature = $temperature;
    }

    public function attach(SplObserver $observer)
    {
        $this->observers->attach($observer);
    }

    public function detach(SplObserver $observer)
    {
        $this->observers->detach($observer);
    }

    public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
}
