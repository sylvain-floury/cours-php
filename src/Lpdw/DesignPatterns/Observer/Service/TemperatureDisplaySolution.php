<?php

namespace Lpdw\DesignPatterns\Observer\Service;

use SplSubject;

class TemperatureDisplaySolution implements \SplObserver
{
    private $ambientTemperature;

    public function update(SplSubject $thermometer)
    {
        $this->ambientTemperature = $thermometer->getTemperature();
    }

    public function getAmbientTemperature():float
    {
        return $this->ambientTemperature;
    }
}
