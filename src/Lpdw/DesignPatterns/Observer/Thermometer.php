<?php

namespace Lpdw\DesignPatterns\Observer;

use SplObjectStorage;
use SplObserver;

class Thermometer implements \SplSubject
{
    private $temperature;

    private $observers;

    public function __construct()
    {
        // TODO
    }

    public function getTemperature():float
    {
        return $this->temperature;
    }

    public function setTemperature(float $temperature)
    {
        $this->temperature = $temperature;
    }

    public function attach(SplObserver $observer)
    {
        // TODO
    }

    public function detach(SplObserver $observer)
    {
        // TODO
    }

    public function notify()
    {
        // TODO
    }
}
