<?php

namespace Lpdw\DesignPatterns\Proxy;

class ProxyFile implements SubjectFile
{

    private $filePath;
    private $realFile;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    public function getContent(): string
    {
        if($this->realFile == null) {
            $this->realFile = new RealFile($this->filePath);
        }

        return $this->realFile->getContent();
    }

    public function getSize(): int
    {
        return filesize($this->filePath);
    }
}