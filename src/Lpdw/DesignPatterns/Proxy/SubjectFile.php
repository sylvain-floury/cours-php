<?php

namespace Lpdw\DesignPatterns\Proxy;

interface SubjectFile
{
    public function getContent():string;
    public function getSize():int;
}
