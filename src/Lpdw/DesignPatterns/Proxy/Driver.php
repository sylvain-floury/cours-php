<?php

namespace Lpdw\DesignPatterns\Proxy;


class Driver
{
    private $age;

    public function __construct(int $age)
    {
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        $this->age = $age;
    }
}