<?php

namespace Lpdw\DesignPatterns\Proxy;


class ProxyCar implements SubjectCar
{
    private $driver;
    private $realCar;

    /**
     * ProxyCar constructor.
     * @param $driver
     */
    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }


    public function driveCar()
    {
        if($this->driver->getAge() < 16) {
            throw new \InvalidArgumentException('Driver is too young!');
        }

        if($this->realCar == null)  {
            $this->realCar = new RealCar();
        }

        return $this->realCar->driveCar();
    }
}