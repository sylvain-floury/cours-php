<?php

namespace Lpdw\DesignPatterns\Iterator\Collection;

use ArrayIterator;
use Iterator;

class PersonCollectionIteratorAggregate implements \IteratorAggregate
{
    private $collection;

    public function __construct(array$collection)
    {
        $this->collection = $collection;
    }

    public function getIterator():Iterator
    {
        return new ArrayIterator($this->collection);
    }
}
