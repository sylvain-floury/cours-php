<?php

namespace Lpdw\DesignPatterns\Iterator\Collection;

use Lpdw\DesignPatterns\Iterator\Model\Person;

class PersonCollectionSolution implements \Iterator
{
    private $currentIndex;

    private $collection;

    public function __construct(array $collection)
    {
        $this->collection = $collection;
        $this->rewind();
    }

    public function current():?Person
    {
        return $this->collection[$this->currentIndex];
    }

    public function next()
    {
        $this->currentIndex++;
    }

    public function key():int
    {
        return $this->currentIndex;
    }

    public function valid():bool
    {
        return array_key_exists($this->currentIndex, $this->collection);
    }

    public function rewind()
    {
        $this->currentIndex = 0;
    }
}
