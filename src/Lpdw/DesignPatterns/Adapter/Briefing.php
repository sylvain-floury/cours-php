<?php

namespace Lpdw\DesignPatterns\Adapter;

interface Briefing
{
    public function addInstruction(string $instruction);

    public function getAllInstructions():string;
}
