<?php

namespace Lpdw\DesignPatterns\Adapter;

interface Speech
{
    public function readText():string;
}
