<?php

namespace Lpdw\DesignPatterns\Adapter;

class BriefingAdapter implements Speech
{
    private $briefing;

    public function __construct(Briefing $briefing)
    {
        $this->briefing = $briefing;
    }

    public function readText():string
    {
        return $this->briefing->getAllInstructions();
    }
}
