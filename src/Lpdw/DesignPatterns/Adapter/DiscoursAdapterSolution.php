<?php

namespace Lpdw\DesignPatterns\Adapter;

class DiscoursAdapterSolution implements Speech
{
    private $discours;

    public function __construct(Discours $discours)
    {
        $this->discours = $discours;
    }

    public function readText():string
    {
        return $this->discours->lireTexte();
    }
}
