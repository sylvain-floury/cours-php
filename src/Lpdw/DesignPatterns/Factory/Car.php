<?php

namespace Lpdw\DesignPatterns\Factory;

class Car implements Vehicle
{
    private $color;

    public function countWheels():int
    {
        return 4;
    }

    public function setColor(string $color)
    {
        $this->color = $color;
    }

    public function getColor():string
    {
        return $this->color;
    }
}
