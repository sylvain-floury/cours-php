<?php

namespace Lpdw\DesignPatterns\Factory;

abstract class Factory
{
    abstract public function create():Vehicle;

    public function createWithColor(string $color):Vehicle
    {
        $vehicle = $this->create();
        $vehicle->setColor($color);
        return $vehicle;
    }
}
