<?php

namespace Lpdw\DesignPatterns\Factory;

interface Vehicle
{
    public function countWheels():int;

    public function setColor(string $color);

    public function getColor():string;
}
