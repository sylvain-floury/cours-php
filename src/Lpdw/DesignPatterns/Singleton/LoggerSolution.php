<?php

namespace Lpdw\DesignPatterns\Singleton;

class LoggerSolution
{
    private static $instance;

    public static function getInstance():LoggerSolution
    {
        if (null === self::$instance) {
            self::$instance = new LoggerSolution();
        }

        return self::$instance;
    }

    private function __construct()
    {
    }

    public function log(string $level, string $message)
    {
        // Code pour enregistrer le message du log.
    }
}
