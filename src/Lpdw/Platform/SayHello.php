<?php

namespace Lpdw\Platform;

class SayHello
{
    public static function toSomeone($name) {
        return 'Hello ' + $name;
    }
}