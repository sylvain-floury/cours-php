<?php

namespace Lpdw\Platform;

class StringUtils
{
    public static function capitalize($string) {
        return strtoupper($string);
    }
}