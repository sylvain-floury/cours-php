<?php


namespace kata;


class ArrayUtils
{

    public static function contains(array $array, $element)
    {
        return in_array($element, $array, true);
    }
}
