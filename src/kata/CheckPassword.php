<?php

namespace kata;

class CheckPassword
{

    public static function isValid(string $password):bool
    {
        if(empty($password)) {
            return false;
        }

        if(strlen($password) < 8) {
            return false;
        }

        if(!preg_match('/\d/', $password)) {
            return false;
        }

        if(!preg_match('/[A-Z]/', $password)) {
            return false;
        }

        if(!preg_match('/[a-z]/', $password)) {
            return false;
        }

        if(!preg_match('/\W/', $password)) {
            return false;
        }

        return true;
    }
}
