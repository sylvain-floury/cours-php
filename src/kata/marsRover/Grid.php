<?php


namespace kata\marsRover;


class Grid
{
    public int $abscissa;
    public int $ordinate;

    /**
     * Grid constructor.
     * @param int $abscissa
     * @param int $ordinate
     */
    public function __construct(int $abscissa, int $ordinate)
    {
        $this->abscissa = $abscissa;
        $this->ordinate = $ordinate;
    }

}
