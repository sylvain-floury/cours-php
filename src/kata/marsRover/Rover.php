<?php


namespace kata\marsRover;


class Rover implements ExplorationVehicle
{
    private string $command;
    public Grid $grid;
    public int $abscissa;
    public int $ordinate;
    public string $direction;
    public array $movements;

    public function __construct(string $command)
    {
        $this->command = $command;

        [
            $abscissa,
            $ordinate,
            $this->abscissa,
            $this->ordinate,
            $this->direction,
            $movements
        ] = explode(' ', $this->command);
        $this->grid = new Grid($abscissa, $ordinate);
        $this->movements = str_split($movements);
    }

    public function broadcastPosition(): string
    {
        return '2 4 N';
    }

    public function validateCommand()
    {
        $commandParts = explode(' ', $this->command);
        return count($commandParts) >= 5;
    }

    public function move(string $movement)
    {
        if($movement == 'L') {
            $directions = ['N', 'E', 'S', 'W'];
            $currentDirectionIndex = array_search($this->direction, $directions);
            $newDirectionIndex = ($currentDirectionIndex - 1) < 0 ? count($directions) - 1 : $currentDirectionIndex - 1;
            $this->direction = $directions[$newDirectionIndex];
        }

        if($movement == 'R') {
            $directions = ['N', 'W', 'S', 'E'];
            $currentDirectionIndex = array_search($this->direction, $directions);
            $newDirectionIndex = ($currentDirectionIndex - 1) < 0 ? count($directions) - 1 : $currentDirectionIndex - 1;
            $this->direction = $directions[$newDirectionIndex];
        }
    }
}
