<?php


namespace kata\marsRover;


interface ExplorationVehicle
{
    public function __construct(string $command);
    public function broadcastPosition(): string;
}
