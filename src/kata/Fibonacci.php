<?php


namespace kata;


class Fibonacci
{
    static $memo = [];

    public static function compute(int $a)
    {
        if($a == 1 || $a == 2) {
            return 1;
        }

        return (self::compute($a - 1) + self::compute($a - 2));
    }

    public static function computeMemo(int $a)
    {
        if(array_key_exists($a, self::$memo)) {
            return self::$memo[$a];
        }

        if($a == 1 || $a == 2) {
            return 1;
        }

        return self::$memo[$a] = (self::computeMemo($a - 1) + self::computeMemo($a - 2));
    }
}
