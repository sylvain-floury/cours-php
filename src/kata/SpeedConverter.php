<?php


namespace kata;


class SpeedConverter
{
    public static function convert(float $kmPerHour):int
    {
        return $kmPerHour / (3600 / 1000);
    }
}
