<?php


namespace kata;


class NumberUtils
{

    public static function isEven($int)
    {
        if($int === 0 || !is_integer($int)) {
            return false;
        }
        return $int % 2 === 0;
    }
}
