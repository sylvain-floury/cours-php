= Réalisation d'un blog
:toc:
:icons: font
:numbered:
:source-highlighter: coderay
Sylvain Floury <sylvain@floury.name>; Julien Fredon <julien.fredon@gmail.com>
v1.0, 2018-11-22

Ce document peut être utilisé suivant les conditions de la licence http://www.gnu.org/licenses/fdl-1.3.txt[GNU Free Documentation License Version 1.3]

<<<

== Partie 1

=== Modèle

==== Création d'une entité `Article`.

* [ ] *À quoi va servir l'entité ?* +
À stocker les données métier.

==== Création du repository des articles

. Création du repository `ArticleRepository`.

* [ ] *Quel est le rôle du repository ?* +
Il centralise les échanges avec la couche de données.

. Utilisation de `ArrayCollection`
* [ ] Montrer qu'elle dépend des interfaces `Collection` -> `Iterator` qui permet l'utilisation du `foreach`.
* [ ] L'assertion `assertCount()` est utilisable car dépend de l'interface `Countable`.


=== Controller

. Le constructeur prend le repository en paramètre.
. Grace au type hinting le *Service Container* saura comment réaliser l'injection de dépendance.

* [ ] *Que cherchons nous à réduire avec l'injection de dépendance ?* +
R : Le couplage?

* [ ] Quel principe SOLID pourrions-nous utiliser pour limiter encore plus le couplage ? +
R : *Dependency Inversion* ici nous sommes encore couplés à une implémentation.

. Mise en place des tests fonctionnels
* [ ] *Explication des différentes étapes des tests (vérification du statut 200, ...)*
* [ ] *Différence entre tests fonctionnels et unitaires ?*
* [ ] *Pourquoi le test vérifie le code 200 ?*
* [ ] *À quoi fait référence le selecteur `.article` ?*

=== Twig

. Utilisation Twig pour ne plus avoir la couche présentation dans le controller.
* [ ] *Limitationdu couplage entre la vue et le  controller.*

== Partie 2

=== Utilisation de la BDD

. Modification de `ArticleRepository`.
* [ ] Transformation des Map en liste d'objets.
* [ ] Explication de hydrateRowData et des `call_user_func()`.

. Test unitaire
* [ ] *Rôle des méthodes setUp() et tearDown() ?*
* [ ] *Tranformation du Map en objet au debugger.*
* [ ] *Quel est le problème ? Pourquoi createdAt est null ?*

. Test fonctionnel
* [ ] *Vérification qu'il fonctionne toujours.*

=== Service de consultation

. Création du service
* [ ] *Décorrèle la  logique métier.*
* [ ] *Quel est le principe SOLID mise en oeuvre ici ?*
* [ ] *Explication de `str_replace()`*

. Modification du controller
* [ ] *Attention au changement de paramètre dans le controller*

. Vérifier en rejouant les tests
* [ ] *Quel est le problème de la version donnée de `ArticleService` ?* +
R : Code mort de la connexion.

=== Travail à réaliser

* [ ] *Présentation des routes*
* [ ] *Présentation des `Request`*
* [ ] *Champs de formulaire sous forme de tableau*