== Espace de travail

. Créez le dossier _seance-07_
. Clonez le dépôt https://gitlab.com/LPDW/infrastructures-docker depuis le dossier _seance-07_.

[source, bash]
----
$ git clone git@gitlab.com:LPDW/infrastructures-docker.git tp-03
----

[start=3]
. Importez le projet _tp-03_ dans PHPStorm

== Désactivation de SELinux

La version de Fedora fournie nécessite de désactiver SELinux pour que Docker fonctionne correctement.

Pour cela vous devez éditer le fichier `/etc/selinux/config` en tant que root.

[source, bash]
----
# vi /etc/selinux/config
----

Mettez la valeur `SELINUX` à `disabled` comme dans l'exemple suivant :

[source, bash]
----
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=disabled
# SELINUXTYPE= can take one of these three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
----

Redémarrez la VM et vérifiez que SELinux est bien désactivé.

[source, bash]
----
# sestatus
SELinux status:                 disabled
----

<<<

== Présentation des éléments à disposition.

Vous trouverez deux infrastructures basées sur la technologie de conteneurs Docker :

* apache_mod-php
* apache_php-fpm

Ces conteneurs nous permettront de simuler différentes configurations serveurs. +
Ils sont basés sur la distribution Linux Debian.

La première `apache_mod-php` emule un serveur utilisant une configuration Apache et PHP obsolète mais encore très répendue. +
La seconde infrastructure `apache_php-fpm` emule une méthode bien plus moderne et performante pour faire dialoguer un serveur Web et PHP.

=== Utilisation

==== apache_mod-php

[source,bash]
----
# Se placer dans le répertoire de l’infrastructure
cd apache_mod-php

# Lancer les conteneurs
docker-compose up -d

# Lister les conteneurs actifs
docker container ls

# Arréter l'infrastructure
docker-compose stop

# Relancer l'infrastructure
docker-compose start
----

Si tout s’est bien déroulé votre terminal devrait afficher un conteneur nommé infra_apache_mod-php.

[source,bash]
----
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                NAMES
acfe3344d9e6        apachemodphp_apache   "/usr/sbin/apache2..."   1 second ago        Up 4 seconds        0.0.0.0:80->80/tcp   infra_apache_mod-php
----


==== apache_php-fpm

La méthode pour déployer cette infrastructure est identique. Il est néanmoins nécessaire de stopper la précédente pour éviter des conflits sur les ports utilisés.

[source,bash]
----
# Se placer dans le répertoire de l’infrastructure
cd apache_php-fpm

# Lancer les conteneurs
docker-compose up -d

# Lister les conteneurs actifs
docker container ls

# Arréter l'infrastructure
docker-compose stop

# Relancer l'infrastructure
docker-compose start
----

Cette fois, si tout s’est bien déroulé vous devriez constater l’affichage de deux conteneurs : *infra_apache* et *infra_php-fpm*.

[source,bash]
----
CONTAINER ID        IMAGE                  COMMAND                  CREATED                  STATUS              PORTS                    NAMES
492ffaf85b59        apachephpfpm_php-fpm   "/usr/sbin/php-fpm..."   Less than a second ago   Up 3 seconds        0.0.0.0:9000->9000/tcp   infra_php-fpm
055b80f3e6c2        apachephpfpm_apache    "/usr/sbin/apache2..."   Less than a second ago   Up 2 seconds        0.0.0.0:80->80/tcp       infra_apache

----
