= Programmation Orientée Objet - partie 1 - Questionnaire
:toc:
:icons: font
:numbered:
:source-highlighter: coderay
Sylvain Floury <sylvain@floury.name>
v1.0, 2018-09-30

Ce document peut être utilisé suivant les conditions de la licence http://www.gnu.org/licenses/fdl-1.3.txt[GNU Free Documentation License Version 1.3]

<<<

== Présentation

Ce document nécessite d'avoir lu le document Programmation Orientée Objet - partie 1.

== Questions

. Question ?
* [ ] a
* [x] b
* [ ] c
* [ ] d
