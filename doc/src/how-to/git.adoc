= Présentation de Git
:toc:
:icons: font
:numbered:
:source-highlighter: coderay
Sylvain Floury <sylvain@floury.name>
v1.0, 2016-10-12

Ce document peut être utilisé suivant les conditions de la licence http://www.gnu.org/licenses/fdl-1.3.txt[GNU Free Documentation License Version 1.3]

== Création du dépôt local

Commencez par créer un dossier qui va accueillir le dépôt local.

[source, bash]
----
$ mkdir demo-git <1>
$ cd demo-git <2>
$ git init <3>
Dépôt Git vide initialisé dans /home/sylvain/tmp/demo-git/.git/
----
<1> Création du dossier qui va accueillir le dépôt.
<2> Placez vous dans le dossier.
<3> Intialisation du dépôt Git.

Ajoutez un fichier README.md.

[source, text]
.README.md
----
Fichier README qui décrit votre dépôt.
----

Vérifiez l'état du dépôt avec la commande `git status`.

[source, bash]
----
$ git status
Sur la branche master

Validation initiale

Fichiers non suivis:
  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)

        README.md

aucune modification ajoutée à la validation mais des fichiers non suivis sont présents (utilisez "git add" pour les suivre)
----

Le fichier README.md est marqué comme non suivi par Git. Il ne sera donc pas pris en compte lors du commit.
Pour changer cela utilisez la commande `git add`.

[source, bash]
----
$ git add .
$ git status                                                                                                                                                                                                                                    
Sur la branche master                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                              
Validation initiale                                                                                                                                                                                                                                                           
                                                                                                                                                                                                                                                                              
Modifications qui seront validées :                                                                                                                                                                                                                                           
  (utilisez "git rm --cached <fichier>..." pour désindexer)                                                                                                                                                                                                                   
                                                                                                                                                                                                                                                                              
        nouveau fichier : README.md                                                                                                                                                                                                                                           
                                          
----

Vous pouvez maintenant réaliser votre premier commit.

[source, bash]
----
$ git commit <1>
[master (commit racine) 579c14f] Commit initial                                                                                                                                                                                                                               
 1 file changed, 0 insertions(+), 0 deletions(-)                                                                                                                                                                                                                              
 create mode 100644 README.md
----

<1> La commande commit vous enverra vers un éditeur pour vous permettre de saisir un message. Saisissez un message comme celui-ci : Commit initial.

Comme vous l'indique la commande `git status` vous êtes sur la branche *master*.

Pour connaître la branche courante utilisez la commande `git branch`.

[source, bash]
----
$ git branch                                                                                                                                                                                                                                     
* master        <1>                
----
<1> La branche courante est préfixée du caractère ***.


== Création du dépôt distant

Connectez-vous au Gitlab de l'université avec vos identifiants (https://git.unilim.fr).

Depuis la page d'accueil cliquez sur le bouton *New Project*.

Saisissez le nom du projet `demo-git` et choisissez la visibilité que vous souhaitez.

Revenez dans votre dépôt local pour le lier à ce dépôt avec la commande suivante.

[source, bash]
----
$ git remote add origin git@git.unilim.fr:flourv01/demo-git.git <1>
----
<1> Adaptez l'url avec celle de votre dépôt.

Vous pouvez maintenant envoyer vos modifications grâce à la commande `git push`.

[source, bash]
----
$ git push origin master                                                                                                                                                                                                                         
Enter passphrase for key '/home/sylvain/.ssh/id_rsa':                                                                                                                                                                                                                         
Décompte des objets: 3, fait.                                                                                                                                                                                                                                                 
Écriture des objets: 100% (3/3), 217 bytes | 0 bytes/s, fait.                                                                                                                                                                                                                 
Total 3 (delta 0), reused 0 (delta 0)                                                                                                                                                                                                                                         
To git.unilim.fr:flourv01/demo-git.git                                                                                                                                                                                                                                        
 * [new branch]      master -> master
----
