<?php

namespace SellingPrice;

class CalculationService
{
    const PRODUCT_PROFIT_MARGIN = 1.15;
    const FILES_REPOSITORY = __DIR__ . '/selling-prices/';

    public function process()
    {
        $supplierPriceList = $this->getSupplierPrice();
        $productFinalPrice = $this->calculateSellingPrice($supplierPriceList);
        $this->saveSellingPrice($productFinalPrice);
    }

    private function getSupplierPrice(): mixed
    {
        $supplierPriceData = file_get_contents('http://localhost/pricedata.json');
        $supplierPriceList = json_decode($supplierPriceData, true);

        return $supplierPriceList;
    }

    private function calculateSellingPrice($supplierPriceList): array
    {
        $productFinalPrice = [];
        foreach ($supplierPriceList as $productLabel => $productPrices) {
            $productFinalPrice[$productLabel] = number_format(array_sum($productPrices) / count($productPrices) * PRODUCT_PROFIT_MARGIN,
                2);
        }

        return $productFinalPrice;
}

    private function saveSellingPrice(array $productFinalPrice): void
    {

        if ( ! is_dir(self::FILES_REPOSITORY)) {
            mkdir(self::FILES_REPOSITORY);
        }
        $filePath    = self::FILES_REPOSITORY . date('Y-m-d') . '.json';
        $fileContent = json_encode($productFinalPrice);
        file_put_contents($filePath, $fileContent);
    }
}