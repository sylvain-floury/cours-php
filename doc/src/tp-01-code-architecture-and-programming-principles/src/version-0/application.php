<?php

use SellingPrice\CalculationService;

require __DIR__ . '/vendor/autoload.php';

$SellingPriceCalculationService= new CalculationService();
$SellingPriceCalculationService->process();