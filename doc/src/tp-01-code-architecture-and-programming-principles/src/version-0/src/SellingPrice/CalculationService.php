<?php

namespace SellingPrice;

class CalculationService
{
    public function process()
    {
        // Get supplier prices list from local file
        $buffer = file_get_contents(__DIR__ . '/../../resources/suppliers/pricedata.json');
        $list   = json_decode($buffer, true);
        $i      = 0;

        // Calculate ours selling prices
        $productFinalPrice = [];
        foreach ($list as $l => $prices) {
            //$productFinalPrice[$l] = number_format(array_sum($priceList) / count($priceList) * 1.10, 2);
            $productFinalPrice[$l] = number_format(array_sum($prices) / count($prices) * 1.15, 2);
            //$productFinalPrice[$label] = number_format(array_sum($prices) / count($prices) * 1.12, 2);
        }

        // Save result in file
        if ( ! is_dir(__DIR__ . '/../../resources/selling-prices/')) {
            mkdir(__DIR__ . '/../../resources/selling-prices/');
        }
        $filePath    = __DIR__ . '/../../resources/selling-prices/' . date('Y-m-d') . '.json';
        $fileContent = json_encode($productFinalPrice);
        file_put_contents($filePath, $fileContent);
    }
}
