<?php

use SellingPrice\CalculationService;
use SellingPrice\PriceProvider\SupplierPriceFromLocalFile;
use SellingPrice\Storage\CsvFile;
use SellingPrice\Storage\JsonFile;

require '../vendor/autoload.php';

$supplierPriceFromInternalWebService = new SupplierPriceFromLocalFile();

$sellingPriceLocalFileJsonStorage = new JsonFile(__DIR__ . '/repository-for-json/');
$sellingPriceLocalFileCsvStorage  = new CsvFile(__DIR__ . '/repository-for-csv/');

$sellingPriceService = new CalculationService($supplierPriceFromInternalWebService);
$sellingPriceService->process($sellingPriceLocalFileJsonStorage);
$sellingPriceService->process($sellingPriceLocalFileCsvStorage);