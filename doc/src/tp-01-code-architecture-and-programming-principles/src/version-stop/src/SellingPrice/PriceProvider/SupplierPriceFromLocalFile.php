<?php

namespace SellingPrice\PriceProvider;

class SupplierPriceFromLocalFile
{
    public function retrieve(): array
    {
        $supplierPriceData = file_get_contents('http://localhost/pricedata.json');

        return json_decode($supplierPriceData);
    }
}