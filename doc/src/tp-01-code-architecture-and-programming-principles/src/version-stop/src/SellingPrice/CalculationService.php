<?php

namespace SellingPrice;

use SellingPrice\PriceProvider\SupplierPriceFromLocalFile;
use SellingPrice\Storage\CanSave;

class CalculationService
{
    const PRODUCT_PROFIT_MARGIN = 1.15;

    private $supplierPriceFromLocalFile;

    public function __construct(SupplierPriceFromLocalFile $supplierPriceFromLocalFile)
    {
        $this->supplierPriceFromLocalFile = $supplierPriceFromLocalFile;
    }

    public function process(CanSave $sellingPriceStorage)
    {
        $sellingPrice = $this->calculate();
        $sellingPriceStorage->save($sellingPrice);
    }

    public function calculate()
    {
        $supplierPriceList = $this->supplierPriceFromLocalFile->retrieve();

        $productFinalPrice = [];
        foreach ($supplierPriceList as $productLabel => $productPrices) {
            $productFinalPrice[$productLabel] = number_format(array_sum($productPrices) / count($productPrices) * self::PRODUCT_PROFIT_MARGIN,
                2);
        }

        return $productFinalPrice;
    }
}