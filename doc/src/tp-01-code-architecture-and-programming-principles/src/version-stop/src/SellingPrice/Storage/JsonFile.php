<?php

namespace SellingPrice\Storage;

class JsonFile implements CanSave
{
    private $repository;

    public function __construct(string $repository)
    {
        $this->repository = $repository;
    }

    public function save(array $sellingPrice)
    {
        if ( ! is_dir($this->repository)) {
            mkdir($this->repository);
        }
        $filePath    = $this->repository . '/' . date('Y-m-d') . '.json';
        $fileContent = json_encode($sellingPrice);
        file_put_contents($filePath, $fileContent);
    }
}
