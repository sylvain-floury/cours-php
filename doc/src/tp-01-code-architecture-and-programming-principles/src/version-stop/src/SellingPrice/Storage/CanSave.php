<?php

namespace SellingPrice\Storage;

interface CanSave
{
    public function save(array $sellingPrice);
}