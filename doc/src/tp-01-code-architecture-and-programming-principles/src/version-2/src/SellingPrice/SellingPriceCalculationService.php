<?php

namespace SellingPrice;

class CalculationService
{
    const PRODUCT_PROFIT_MARGIN = 1.15;

    public function process()
    {
        $supplierPriceFromInternalWebService = new SupplierPriceFromInternalWebService();
        $supplierPriceList = $supplierPriceFromInternalWebService->retrieve();

        $productFinalPrice = $this->calculateSellingPrice($supplierPriceList);

        $sellingPriceFileJsonStorage = new JsonFile();
        $sellingPriceFileJsonStorage->save($productFinalPrice);
    }

    private function calculateSellingPrice($supplierPriceList): array
    {
        $productFinalPrice = [];
        foreach ($supplierPriceList as $productLabel => $productPrices) {
            $productFinalPrice[$productLabel] = number_format(array_sum($productPrices) / count($productPrices) * self::PRODUCT_PROFIT_MARGIN,
                2);
        }

        return $productFinalPrice;
    }
}