<?php

use SellingPrice\CalculationService;

require '../vendor/autoload.php';

$SellingPriceCalculationService= new CalculationService();
$SellingPriceCalculationService->process();