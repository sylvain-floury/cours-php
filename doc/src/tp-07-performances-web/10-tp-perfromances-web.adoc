= Performances d'une application PHP
:toc:
:icons: font
:numbered:
:source-highlighter: coderay
Sylvain Floury <sylvain@floury.name>; Julien Fredon <julien.fredon@gmail.com>
v1.2, 2020-01-24

Ce document peut être utilisé suivant les conditions de la licence http://www.gnu.org/licenses/fdl-1.3.txt[GNU Free Documentation License Version 1.3]

<<<

== Description

Le but de ce TP est d'aborder la question de la répartition de charge.

Pour cela nous allons déployer plusieurs instances d'une même application sur plusieurs serveurs web.

Nous nous limiterons à cet aspect web  et nous considèrerons que toutes les instances de l'application appellent la même base de données.
Nous considèrerons également que la base de données est dimensionnée pour répondre à la solicitation de toutes les instances.

== Application de base

Nous allons utiliser une application de blog existante.
Récupérez cette application sur le serveur Moodle.


== La répartition de charge

=== Présentation de la maquette

*ApacheBench*

Pour simuler de la charge nous utiliserons ApacheBench.

Cet outil va nous permettre d’envoyer un nombre défini de requêtes vers notre application et de mesurer les temps de réponse.

*Serveur web*

Côté serveur web nous utiliserons le serveur interne de PHP (built-in server).
Il présente l'intérêt d'utiliser un seul processus et nous permettra plus facilement d'illustrer le problème d'engorgement.

=== Architecture de base

==== Initialisation de l'application

Après avoir décompresser l'archive `tp-performances-web-base.zip` placez-vous dans le dossier `tp-performances-web-base` et lancez la commande
suivante.

[source,bash]
----
$ docker-compose run --rm composer composer install -d ./blog
----

==== Utilisation de l'application


Nous allons dans un premier temps lancer l'application blog sur une seule instance.

Pour cela utilisez la commande suivante :

[source,bash]
----
$ docker-compose up -d blog
----

Nous utiliserons l'outil `ab` (_ApacheBench_) qui permet de lancer un certain nombre de requêtes sur une même adresse.
Il nous fournit ensuite les temps de réponse obtenus.

[source,bash]
----
$ docker-compose run --rm apache ab -n 100 http://blog:8000/
...
Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       0
Processing:    24   25   1.2     25      35
Waiting:       24   25   1.2     25      34
Total:         24   25   1.2     25      35
...
----

La commande va lancer 100 requêtes les unes à la suite des autres.

Le résultat obtenu va nous servir de référence.

[TIP]
====
Comme vous utilisez Docker l'application web correspond au service `blog` dans le fichier `docker-compose.yml`.
====

Augmentons le nombre de requêtes concurrentes.

[source,bash]
----
$ docker-compose run --rm apache ab -c 20 -n 100 http://blog:8000/
...
Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.3      0       1
Processing:    29  443 111.0    487     500
Waiting:       29  443 111.0    486     499
Total:         30  443 110.7    487     500
----

Cette commande lance toujours 100 requêtes mais les lance en parallèle 20 par 20.

Le temps de réponse augmente considérablement car le serveur web interne de PHP n'effectue aucune opération en parallèle.

Nous sommes face ici à un problème d'engorgement.

=== Architecture avec 2 serveurs web

Pour palier à ce problème d'engorgement nous allons utiliser un système de répartition de charge (load balancing).

La répartition de charge est une technique permettant de distribuer une charge de travail entre différents serveurs. +
C’est une solution très souvent mise en œuvre pour étendre une infrastructure en ajoutant de nouveaux serveurs
afin de supporter la charge d’une application.
On parle alors d’évolutivité horizontale (horizontal scalability) par opposition à l’évolutivité verticale
(vertical scalability) qui consiste à augementer les ressources des serveurs.

.Répartition de charge sur n serveurs
["graphviz", "../../pdf/images/infrastructure", svg]
----
digraph G { rankdir=TB;
    "Répartiteur de charge" -> "Serveur 1"
    "Répartiteur de charge" -> "Serveur 2"
    "Répartiteur de charge" -> "…"
    "Répartiteur de charge" -> "Serveur n"
}
----

Dans les configurations les plus basiques, chaque requête HTTP reçue par le répartiteur de charge va être transmise à
un serveur web à tour de rôle. Le serveur web va traiter la requête et renvoyer le résultat au répartiteur de charge
qui transmettra la réponse au client à l'origine de la requête.

.Dialogue HTTP entre deux clients et deux serveurs
["graphviz", "../../pdf/images/request-exchange", svg]
----
digraph G {
    "Internaute A" -> "Répartiteur de charge" [label=1,color=red]
    "Internaute B" -> "Répartiteur de charge" [label=5,color=blue]
    "Répartiteur de charge" -> "Serveur 1" [label=2,color=red]
    "Répartiteur de charge" -> "Serveur 2" [label=6,color=blue]
    "Serveur 1" -> "Répartiteur de charge" [label=3,color=red]
    "Serveur 2" -> "Répartiteur de charge" [label=7,color=blue]
    "Répartiteur de charge" -> "Internaute A" [label=4,color=red]
    "Répartiteur de charge" -> "Internaute B" [label=8,color=blue]
}
----

Beaucoup d’outils permettent de faire de la répartition de charge :

* apache,
* nginx,
* varnish,
* HAProxy,
* …

Dans ce TP, nous allons utiliser HAProxy (http://www.haproxy.org/) qui est un répartiteur de charge (load balancer)
spécialisé dans ce domaine et réputé comme très performant. Il permettra de distribuer les requêtes des internautes sur
2 serveurs web faisant fonctionner l’application de blog. +
Le serveur HAProxy et les deux serveurs web seront simulés par des conteneurs Docker.

[NOTE]
====
Pour une description plus complète des possibilités offertes par la répartition de charge,
veuillez vous référer à l’article wikipédia : https://fr.wikipedia.org/wiki/Répartition_de_charge
====

Le fonctionnement sera le suivant :

. Les requêtes arriveront sur HAProxy sur le port 8000.
. HAProxy répartira équitablement les requêtes sur les 2 serveurs.

Pour cela modifiez le fichier docker-compose.yml comme suit.

[source,yaml]
----
version: '3'
services:
  blog1:    <1>
    build: ./docker/blog/
    ports:
      - 8001:8000 <2>
    volumes:
      - "./blog:/usr/src/blog"
    working_dir: /usr/src/blog
    command: php -S 0.0.0.0:8000 -t web/
    depends_on:
      - mysql
  blog2: <3>
      build: ./docker/blog/
      ports:
        - 8002:8000 <4>
      volumes:
        - "./blog:/usr/src/blog"
      working_dir: /usr/src/blog
      command: php -S 0.0.0.0:8000 -t web/
      depends_on:
        - mysql
  mysql:
    image: mysql
    environment:
      MYSQL_ROOT_PASSWORD: lpdw
      MYSQL_DATABASE: silex
      MYSQL_USER: silex
      MYSQL_PASSWORD: silex
    volumes:
      - "./docker/mysql:/docker-entrypoint-initdb.d"
  adminer:
    image: adminer
    ports:
      - 9000:8080

  composer:
    build: ./docker/composer/
    volumes:
      - "./blog:/app/blog"

  apache:
    image: httpd

  haproxy: <5>
    image: haproxy:1.7-alpine
    ports:
      - "8000:80"
    volumes:
      - "./docker/haproxy/haproxy.conf:/usr/local/etc/haproxy/haproxy.cfg"
    depends_on:
      - blog1
      - blog2
----
<1> Renommer le service `blog`.
<2> Modifier le port.
<3> Dupliquer le service `blog`.
<4> Modifier le port.
<5> Ajouter le service `haproxy`.


Nous ne rentrerons pas dans le détail de la configuration de HAProxy, ce n’est pas l’objectif du TP, mais vous
pouvez tout de même jetter un œil sur le fichier de configuration. Les deux parties les plus intéressantes sont :

* la section *frontend* : qui définit les IP et les ports sur lesquels écoute HAProxy et à partir desquels il va recevoir
les requêtes des internautes.
* la section *backend* qui définit principalement les serveurs (adresse IP et port) sur lesquels répartir les
requêtes.

La configuration permet également de définir une adresse particulière à partir de laquelle il sera possible de consulter
des statistiques de fonctionnement. +
Dans notre exemple, cette interface est accessible à l’adresse http://localhost:8000/haproxy avec l’identifiant
`haproxy` et le mot de passe `haproxy`.

[NOTE]
====
HAProxy est un répartiteur de charge généraliste, il fonctionne très bien avec le protocole HTTP mais on peut
tout aussi bien s’en servir pour faire de la répartition de charge avec d’autres protocoles.
====

Lancez les différents services avec la commande suivante :

[source,bash]
----
$ docker-compose up -d
----

Une fois les services lancés, connectez-vous successivement sur les 2 serveurs web pour vérifier leur fonctionnement.

* Ouvrez l'adresse suivante dans un navigateur : http://localhost:8001/
* Ouvrez l'adresse suivante dans un navigateur : http://localhost:8002/

Après avoir vérifié que les serveurs web fonctionnent, utilisez l'adresse http://localhost:8000/.
À l'aide des outils de développement de votre navigateur, recherchez dans l'entête `X-Backend-Server`.

Sa valeur prendra successivement *server1* ou *server2* selon le serveur web appelé.

=== Simulation d'une panne

À présent, votre application repose sur une infrastructure de quatre serveurs :

- 1 répartiteur de charge (reverse proxy) HAProxy,
- 1 serveur de base de données MySQL,
- 2 serveurs web PHP

Dans un premier temps, connectez-vous à l’interface de suivi des statistiques de HAProxy à l’adresse http://localhost:8000/haproxy.
Vous trouverez les identifiants de connexion ainsi que le paramétrage de l’interface dans le fichier de configuration `haproxy.conf`.

L’interface de HAProxy affichera deux sections :

. la partie _frontend_ qui désigne le point d’entrée où les requêtes des internautes arrivent quand ils accèdent à l’adresse http://localhost:8000
. la partie _backend_ qui désigne les points d’échange entre HAProxy et les serveurs web avec lesquels il dialogue.

.Échanges entre les internautes, le répartiteur de charge et les serveurs web
image::resources/mermaid-diagram-20200123221741.svg[]

Simulons une panne serveur en stoppant l’un des conteneurs PHP :
[source,bash]
----
$ docker-compose stop blog1
----

Rafraichissez l’interface de suivi des statistiques de HAProxy, la ligne _backend_ correspondante au serveur devient rouge.
Cela indique que HAProxy ne peut plus communiquer avec ce dernier.

Pourtant, si vous rafraichissez la page de votre application à l’adresse http://localhost:8000, vous obtenez toujours une reponse ;
aucun message d’erreur n’apparait. Le load balancer ayant détecté un dysfonctionnement du serveur, il ne lui transmet plus aucune requète et
préfère les diriger vers le serveur bien portant.

Redemarrez votre serveur en panne avec la commande suivante.

[source,bash]
----
$ docker-compose start blog1
----

Regardez ensuite l’état de votre infrastructure via l’interface de HAProxy.

Stoppez maintenant les deux serveurs PHP et constatez le résultat : votre application est Hors Service.
La panne est générale et le répartiteur de charge n’a plus de solution pour redirigez les requêtes.

Vous pouvez à présent redémarrer vos deux serveurs pour remettre en état votre application.

[NOTE]
====
Votre infrastructure repose sur un seul serveur de base de données.
Cet élément constitue donc ce qu’on nomme un *SPOF*.

Recherchez la définition de cet acronyme anglais.
====

<<<

== Sessions

Distribuer une application sur plusieurs serveurs web amène un certain nombre de problématiques supplémentaires.
Nous allons aborder l'une d'entre elles : la gestion des sessions.

=== Rappels sur les sessions

Les sessions permettent de conserver des informations relatives à un utilisateur entre 2 appels.
Pour cela un identifiant est attribué (_PHPSESSIONID_) au premier appel.
Lors des appels suivants le serveur PHP pourra identifier l'utilisateur grâce à ce _PHPSESSIONID_.

Le mécanisme des sessions reposent généralement sur les cookies pour conserver le _PHPSESSIONID_ côté navigateur.


=== Application utilisée

Pour illustrer le fonctionnement, nous allons ajouter une page qui simule une authentification à l'application.

Vous pouvez récupérer la version modifiée sur le Moodle.

==== Initialisation de l'application

Après avoir décompresser l'archive `tp-performances-web-auth.zip`
placez-vous dans le dossier `tp-performances-web-auth` et lancez la commande suivante.

[source,bash]
----
$ docker-compose run --rm composer composer install -d ./blog
----

==== Modifications apportées

*Configuration de l'application*

Dans le fichier `app.php` l'ajout d'un provider Silex rend les sessions accessibles au travers de ``$app['session']``.

Il repose pour cela sur la classe `Session` du composant Symfony `HttpFoundation`.

[source,php]
.app/app.php
----
$app = new Silex\Application();

$app['debug'] = true;

/**
 * Activation de la gestion des sessions
 */
$app->register(new Silex\Provider\SessionServiceProvider()); <1>

...

$app['blog.controller'] = function() use($app) {
    return new BlogController($app['twig'], $app['blog.message.service'], $app['session']); <2>
};

...
$app['default.controller'] = function() use($app) { <3>
    return new DefaultController($app['twig'], $app['session']);
};
----
<1> Ajout du provider pour la gestion des sessions.
<2> Ajout d'une dépendance de `BlogController` à la gestion des sessions.
<3> Ajout d'un controller pour gérer l'authentification.

*DefaultController*

Ce contrôleur dispose de 3 méthodes que vous retrouvez dans `app/routing.php`.

[source,php]
.app/routing.php
----
<?php
$app->get('/', "default.controller:indexAction"); <1>
$app->get('/login', "default.controller:loginAction"); <2>
$app->post('/auth', "default.controller:authAction"); <3>

...
----
<1> Accueil de l'application
<2> Formulaire d'authentification
<3> Vérification des identifiants

Le fonctionnement est le suivant :

* L'utilisateur essaie d'accéder à l'accueil de l'application.
* L'utilisateur est redirigé vers la page d'authentification (/login).
* L'utilisateur saisit ses identifiants qui sont transmis au traitement d'authentification (/auth).

* Si les identifiants sont correctes.
** Le login de l'utilisateur est enregistré dans la variable de session `user`.
** L'utilisateur est redirigé vers la page d'accueil du blog.

* Si les identifiants sont incorrectes.
** Un message d'erreur est enregistré dans le flashBag de la `Session`.
** L'utilisateur est redirigé vers la page d'authentification.


Cette authentification simpliste utilisera la variable de session `user` pour déterminer si l'utilisateur est connecté.
Si la variable de session `user` existe alors l'utilisateur est considéré comme authentifé.
Dans le cas contraire il est renvoyé vers la page d'authentification.

*BlogController*

Le contrôleur a été modifié pour vérifier que l'utilisateur est authentifié.

[source,php]
.app/routing.php
----
class BlogController
{
    ...

    public function homeAction()
    {
        if (!$this->session->has('user')) { <1>
            return new RedirectResponse('/login');
        }

        $messages = $this->messageService->fetchAll();

        return $this->twig->render('Blog/home.html.twig', [
            'messages' => $messages,
            'user' => $this->session->get('user'),
        ]);
    }
}
----
<1> Contrôle pour vérifier l'authentification

=== Expérimentation des sessions

Lancez le serveur blog1 et vérifiez le fonctionnement.

Une fois authentifié(e), utilisez les outils de développement de votre navigateur pour visualiser le contenu du cookie `PHPSESSID`.
La chaine de caractères représente votre identifiant de session (PHPSESSIONID).

Connectez-vous ensuite au conteneur `blog1`.

Affichez ensuite le contenu de votre fichier de session.
Le fichier est présent dans le dossier `/tmp`, son nom correspond à la valeur de votre cookie PHPSESSIONID prefixé de _sess__.

Dans l'exemple suivant le contenu du cookie PHPSESSID = *3877516e272dfb6326cb6857178e547b*.

[source,bash]
----
$ docker-compose exec blog1 bash
# cat /tmp/sess_3877516e272dfb6326cb6857178e547b
----

Le contenu du fichier est la version serialisée des différentes variables de session.

Vous devriez retrouver sous forme sérialisée la variable `user` et son contenu.

=== Utilisation des sessions avec une application distribuée

==== La problématique

Lancez le serveur `blog2` et essayez d'accéder à la page d'accueil.
Vous devriez être redirigé(e) vers la page d'authentification.

Ceci est dû au fait que les sessions ne sont pas partagées entre les 2 serveurs.

Nous souhaitons répartir notre application sur plusieurs serveurs sans perte de fonctionnalités.

==== Système de fichiers distribué

Pour régler ce problème nous allons partager les fichiers de session entre les 2 serveurs.

Modifiez pour cela le fichier `docker-compose.yml`.

[source, yaml]
----
blog1:
    build: ./docker/blog/
    ports:
      - 8001:8000
    volumes:
      - "./blog:/usr/src/blog"
      - "./docker/blog/sessions:/tmp" <1>
    working_dir: /usr/src/blog
    command: php -S 0.0.0.0:8000 -t web/
    depends_on:
      - mysql
  blog2:
      build: ./docker/blog/
      ports:
        - 8002:8000
      volumes:
        - "./blog:/usr/src/blog"
        - "./docker/blog/sessions:/tmp" <2>
      working_dir: /usr/src/blog
      command: php -S 0.0.0.0:8000 -t web/
      depends_on:
        - mysql
----
<1> partage du volume pour le blog1
<2> partage du volume pour le blog2

Refaites le test suivant :

* Authentifiez-vous sur l'application `blog1`.
* Accédez à la page d'accueil du `blog2`.
* Vous devriez accéder à la page d'accueil du `blog2` sans vous authentifier de nouveau.

Effectuez de nouveau le test en utilisant HAProxy.
Utilisez les outils de développement de votre navigateur pour vérifier qu'après vous être authentifié(e) vous appelez consécutivement les 2 serveurs web.
Pour cela vérifiez le contenu de l'entête `X-Backend-Server`.

[TIP]
====
Une solution pourrait être de demander à HAProxy de renvoyer toutes les requêtes d'un utilisateur toujours vers le même serveur web.
Cependant dans ce cas si le serveur web utilisé par l'internaute tombe alors sa session serait perdue.
====


