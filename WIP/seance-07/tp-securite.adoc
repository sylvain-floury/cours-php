= TP sécurité
:toc:
Sylvain Floury <sylvain@floury.name>
v1.0, 2016-10-15

Ce document peut être utilisé suivant les conditions de la licence http://www.gnu.org/licenses/fdl-1.3.txt[GNU Free Documentation License Version 1.3]

== Présentation

Nous allons ajouter une couche pour sécuriser notre application de blog.
Ceci passer par la mise en place de comptes utilisateurs et la restriction d'accès pour l'application.

Commençons par quelques concepts généraux.

== Filtrage de données

Il est important de toujours vérifier les données envoyées côté back-end même si elles sont vérifiées côté front-end.

Pour effectuer cette vérification nous allons nous servir de filtres.

Les filtres sont généralement regroupés en 2 catégories les filtres de nettoyage et les filtres de validation.

=== Filtres de nettoyage

Leur rôle est d'enlever les caractères interdits ou dangereux des données fournies. 
Avec ce type de filtre les données sont modifiées.

PHP dispose d'une extension `Filter` spécifique au filtrage des données footnote:[http://php.net/manual/fr/book.filter.php].

Voici un exemple d'utilisation avec la fonction `filter_var` footnote:[http://php.net/manual/fr/function.filter-var.php].

[source, php]
----
<?php

use PHPUnit\Framework\TestCase;

class SanitizeFilterTest extends TestCase
{
    /**
     * @test
     */
    public function shouldSanitizeEmail() {
        $invalidEmail = 'mon Email@test.fr';
        $validEmail = filter_var($invalidEmail, FILTER_SANITIZE_EMAIL);
        $this->assertEquals('monEmail@test.fr', $validEmail);
    }
}
----

Vous trouverez la liste des différents filtres de nettoyage fournis par PHP sur http://php.net/manual/fr/filter.filters.sanitize.php.

=== Filtres de validation

Leur rôle est de valider les données fournies fonction des critères fournis. Ce type de filtre ne modifie pas les données et se content de déterminer leur validité.

[source, php]
----
<?php

use PHPUnit\Framework\TestCase;

class ValidateFilterTest extends TestCase
{
    /**
     * @test
     */
    public function shouldValidateFilter() {
        $invalidEmail = 'mon Email@test.fr';
        $validEmail = filter_var($invalidEmail, FILTER_VALIDATE_EMAIL);
        $this->assertFalse($validEmail);
    }
}
----

Vous trouverez la liste des différents filtres de validation fournis par PHP sur http://php.net/manual/fr/filter.filters.validate.php.

== Empreinte de mots de passe

La fonction de condensation footnote:[https://fr.wikipedia.org/wiki/Fonction_de_hachage] va transformer une chaine de caractères d'une taille arbitraire en une chaine de taille fixe.
Cette chaine de taille fixe est appelée *empreinte* (digest en anglais mais le terme "hash" est souvent utilisé).

Une autre de ses caractéristiques est d'être une fonction à sens unique.
Cela traduit le fait que l'empreinte est simple à calculer mais que l'opération inverse est théoriquement impossible.

PHP fournit plusieurs fonctions qui simplifient la mise en place de ce mécanisme.

* password_hash() : qui calcule l'empreinte footnote:[http://php.net/function.password-hash].
* password_verify() : qui vérifie l'empreinte par rapport à une chaine donnée footnote:[http://php.net/manual/fr/function.password-verify.php].

[source, php]
.tests/Hash/HashingPassword.php
----
<?php
namespace Hash;

use PHPUnit\Framework\TestCase;

class HashingPassword extends TestCase
{
    protected static $passwordHash;

    /**
     * @beforeClass
     */
    public static function init() {
        self::$passwordHash = password_hash('mon mot de passe', PASSWORD_DEFAULT);
    }

    /**
     * @test
     */
    public function shouldValidPassword() {
        $this->assertTrue(password_verify('mon mot de passe', self::$passwordHash));
    }

    /**
     * @test
     */
    public function shouldNotValidPassword() {
        $this->assertFalse(password_verify('un autre mot de passe', self::$passwordHash));
    }
}
----

== Gestion des comptes

Nous allons partir sur la structure suivante.

[source]
----
.
├── app
│   ├── app.php
│   └── routing.php
├── phpunit.xml
├── src
│   ├── Blog
│
   ...
│
│   └── Security
│       ├── Controller
│       │   └── UserController.php
│       ├── Entity
│       │   └── User.php
│       ├── Form
│       │   └── UserForm.php
│       ├── Provider
│       │   ├── UserRepositoryProvider.php
│       │   └── UserServiceProvider.php
│       ├── Repository
│       │   ├── EntityRepository.php
│       │   └── UserRepository.php
│       └── Service
│           └── UserService.php
├── tests
│   └── Security
│       ├── Repository
│       │   └── UserRepositoryTest.php
│       └── Service
│           └── UserServiceTest.php
├── vendor
│   ├── autoload.php
│    
   ...
│   
├── views
│   ├── layout.html.twig
│   └── Security
│       ├── add.html.twig
│       └── index.html.twig
└── web
    └── index.php
----

=== Réalisation de la structure de base

Implémentez les différentes classes du namespace `Security` présentes dans le diagramme suivant.

image::diagrammes/CD-Security.png[]

[NOTE]
====
Les tests unitaires sont absents du diagramme pour des raisons de lisibilité.
Appuyez-vous malgré tout dessus pour réaliser votre implémentation.
====

Vous aurez également besoin de créer les vues de actions `index` et `add`.

Pour l'instant notre l'action `index` se limite à lister les comptes créés et à proposer un formulaire d'inscription (ajout).

Vous devrez également créer et utiliser la table suivante.

image::diagrammes/MPD-Security.png[]

=== Mise en place de la validation

Nous allons passer par un composant de Symfony qui simplifie la validation avec un grand nombre de validations prédéfinies et de mécanismes de validations.

Le principe est le même que ce que nous venons de voir avec les filtres de validation.
Les données sont comparées par rapport à des règles ou filtres pour déterminer leur validité.

Seule la terminologie change et au lieu de parler de filtres nous parlerons de contraintes (en anglais constraints).

Commençons par l'installer.

[source, bash]
----
$ composer require symfony/validator
----

Ajoutez à votre fichier de bootstrap le provider suivant.

[source, php]
.app/app.php
----
$app->register(new Silex\Provider\ValidatorServiceProvider());
----

Nous pouvons maintenant utiliser les contraintes fournies.
La liste complète est disponible à l'adresse suivante : http://symfony.com/doc/master/validation.html#constraints

Nous allons l'utiliser pour vérifier que les champs nom et prénom ont bien été remplis.

[source, php]
.src/Security/Controller/UserController.php
----
...
    public function addAction(Request $request) {
        $user = new User();
        $errors = new ConstraintViolationList();

        if($request->getMethod() == Request::METHOD_POST) {
            $userForm = new UserForm();

            $user = $userForm->handleRequest($request);
            $errors->addAll($this->container['validator']->validate($user->getFirstname(), new Assert\NotBlank(array('message' => 'Firstname should not be blank'))));
            $errors->addAll($this->container['validator']->validate($user->getLastname(), new Assert\NotBlank(array('message' => 'Lastname should not be blank'))));

            if(count($errors) == 0) {
                $this->container['security.user.service']->addUser();
                return new RedirectResponse('/security/users');
            }
        }

        return $this->container['twig']->render('Security/add.html.twig', array('user' => $user, 'errors' => $errors));
    }
...
----

Nous modifions la vue associée pour afficher les erreurs.

[source, html]
.views/Security/add.html.twig
----
{% extends "layout.html.twig" %}

{% block content %}

<h1>Ajouter un utilisateur</h1>

    {% for error in errors %}
    <div class="alert alert-danger alert-dismissable fade in">
        {{ error.getMessage() }}
    </div>
    {% endfor %}
    <form action="/security/users/add" method="post">
        <input type="text" placeholder="firstname" name="user[firstname]" value="{{ user.firstname }}" />
        <input type="text" placeholder="lastname" name="user[lastname]" value="{{ user.lastname }}" />

        ...

        <button type="submit">Créer</button>
    </form>

{% endblock %}
----

==== Travail à réaliser

Ajoutez des contraintes pour vérifier que l'email est obligatoire et valide.

Ajoutez des contraintes pour vérifier que le mot de passe fasse au moins 8 caractères.

Utilisez les tests fonctionnels pour contrôler ces changements.

=== Amélioration de la création d'un utilisateur

Cette solution de validation a un défaut majeur elle est liée au controller.

Il est toujours préférable de faire la validation le plus prés de la donnée.
L'idée est de réduire au maximum le couplage.

Nous allons pour cela la placer au niveau de l'entité.
Un mécanisme de symfony/validator le permet à condition d'implémenter une méthode `loadValidatorMetadata`.

[source, php]
.src/Security/Entity/User.php
----
...
    static public function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('firstname', new Assert\NotBlank(array('message' => 'Firstname should not be blank')));
        $metadata->addPropertyConstraint('lastname', new Assert\NotBlank(array('message' => 'Lastname should not be blank')));

        // Les contraintes que vous avez ajoutées
       ...
    }
}
----

Modifions ensuite le controller.

[source, php]
.src/Security/Controller/UserController.php
----
...
    public function addAction(Request $request) {
        $user = new User();
        $errors = new ConstraintViolationList();

        if($request->getMethod() == Request::METHOD_POST) {
            $userForm = new UserForm();

            $user = $userForm->handleRequest($request);

            $errors = $this->container['validator']->validate($user);
            if(count($errors) == 0) {
                $this->container['security.user.service']->addUser($user);
                return new RedirectResponse('/security/users');
            }
        }

        return $this->container['twig']->render('Security/add.html.twig', array('user' => $user, 'errors' => $errors));
    }
}
----

La différence peut paraitre mineure mais la logique de validation est maintenant au niveau de l'entité.
Ceci facilite les modifications et n'importe quelle couche de l'application peut valider l'entité.

Enfin améliorons la vue pour indiquer le champ concerné par l'erreur.

[source, html]
.views/Security/add.html.twig
----
{% extends "layout.html.twig" %}

{% block content %}

<h1>Ajouter un utilisateur</h1>

    {% for error in errors %}
    <div class="alert alert-danger alert-dismissable fade in">
        {{ error.getPropertyPath() }}: {{ error.getMessage() }}
    </div>
    {% endfor %}
    <form action="/security/users/add" method="post">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="firstname" name="user[firstname]" value="{{ user.firstname }}" />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="lastname" name="user[lastname]" value="{{ user.lastname }}" />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="email" name="user[email]" value="{{ user.email }}" />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="password" name="user[password]" value="{{ user.password }}" />
        </div>

        <button type="submit" class="btn btn-default">Créer</button>
    </form>

{% endblock %}
----

=== Gestion des mots de passe

Nous avons jusqu'à présent utiliser un mot de passe en clair.
Ceci pose des problèmes de sécurité car tout accès par une tierce partie peut compromettre les comptes.
Pour remédier à cela nous allons utiliser le "hashing" de mot de passe.


==== Travail à réaliser

* Modifiez la création d'un utilisateur pour implémenter le "hashing" de mot de passe.
* Ajoutez une nouvelle méthode au service pour vérifier le mot de passe.
* Réalisez les tests nécessaires.

== Sécurisation de l'application de blog.

Nous allons nous appuyer pour cela sur le `SecurityService` de Silex qui repose sur le composant `Security` de Symfony.

Il nous faut tout d'abord l'installer.

[source, bash]
----
$ composer require symfony/security
----

Nous installons également `Twig bridge` qui fournit des fonctions Twig supplémentaires.
Notamment les fonctions `path()` ou `url()` qui simplifieront son utilisation.

[source, bash]
----
$ composer require symfony/twig-bridge
----

Ajoutons le `SecurityServiceProvider` au fichier de bootstrap.

[source, php]
.app/app.php
----
...

// Configuration des droits de l'application
$app['security.firewalls'] = array(
    'login' => array(
        'pattern' => '^/login$',
    ),
    'secured' => array(
        'pattern' => '^.*$',
        'form' => array('login_path' => '/login'),
        'users' => array(
            'admin' => array('ROLE_ADMIN', '$2y$10$3i9/lVd8UOFIJ6PAMFt8gu3/r5g0qeCJvoSlLCsvMTythye19F77a'), // password 'foo'
        ),
        'logout' => array('logout_path' => '/logout', 'invalidate_session' => true),
    ),
);

$app->register(new Silex\Provider\SecurityServiceProvider());

// Gestion des sessions
$app->register(new Silex\Provider\SessionServiceProvider());

...
----

Nous définissons en même temps un élément indispensable : les firewalls.

Les firewalls définissent à l'aide de règles (pattern) les urls de l'application qui seront soumises à une authentification.

Dans cet exemple l'url `/login` ne sera soumise à aucune authentification mais toutes les autres urls le seront.
Ceci est nécessaire pour permettre à l'utilisateur de s'authentifier.

[WARNING]
====
Le système de firewall s'arrête à la première règle qu'il vérifie.
Dans ce cas il est primordial de placer la règle du login en premier sinon elle serait ignorée.
====

La seconde règle indique qu'une authentification est nécessaire pour toute url.

* Le paramètre `form` indique l'url du formulaire d'authentification (formulaire que nous devrons gérer).
* Le paramètre `users` définit les utilisateurs ou comme nous le verrons la méthode d'authentification.
* Enfin le paramètre `logout` nous permet de prendre en charge la déconnexion.

Comme nous venons de la voir il est nécessaire de prendre en charge la vue qui gère l'authentification.

Nous créons pour cela un nouveau controller.

[source, php]
.src/Security/Controller/AuthController.php
----
<?php
namespace Security\Controller;

use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;

class AuthController
{
    protected $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    public function loginAction(Request $request) {
        return $this->container['twig']->render('Security/login.html.twig', array(
            'error'         => $this->container['security.last_error']($request),
            'last_username' => $this->container['session']->get('_security.last_username'),
        ));
    }
}
----

Nous le déclarons au niveau du bootstrap.

[source, php]
.app/app.php
----
...

$app['security.auth.controller'] = function() use($app) {
    return new AuthController($app);
};

...
----

Nous déclarons ensuite la route correspondante.

[source, php]
.app/routing.php
----
$app->get('/login', "security.auth.controller:loginAction");
----

Enfin nous ajoutons la vue.

[source, html]
.views/Security/login.html.twig
----
{% extends "layout.html.twig" %}

{% block content %}
<form action="{{ path('login_check') }}" method="post">
    {{ error }}
    <input type="text" name="_username" value="" />
    <input type="password" name="_password" value="" />
    <input type="submit" />
</form>
{% endblock %}
----

La route `login_check` est fournie par le `SecurityService` à condition de respecter les noms de champ présents dans le formulaire.

Lancez le serveur et vérifiez que vous parvenez à vous authentifier.

=== Mise en place du contrôle d'accès

Nous souhaiterions que la page d'accueil du blog soit visible mais sans la présence des boutons d'ajout, de modification et de suppression des messages.

Nous commençons pour cela par ajouter une règle au niveau du firewall pour autoriser l'affichage de la home.

[source, php]
.app/app.php
----
...

   'login' => array(
        'pattern' => '^/login$',
    ),
    'home' => array(
        'pattern' => '^/$',
    ),
    'secured' => array(
        'pattern' => '^.*$',

...
----

La page est maintenant accessible même sans être authentifié.

Nous créons ensuite une nouvelle action dans le controller `MessageController` pour différencier la liste des messages des utilisateurs anonymes et connectés.

Ajoutons une nouvelle route.

[source, php]
.app/routing.php
----
<?php
$app->get('/', "blog.controller:homeAction");

$app->get('/messages/list', "message.controller:listAction");
$app->post('/messages/add', "message.controller:addAction");

...
----

Puis ajoutons l'action correspondante. Notez que nous réutilisons la même vue.

[source, php]
.src/Blog/Controller/MessageController.php
----
class MessageController
{
...

    public function listAction() {
        $messages = $this->messageService->fetchAll();

        return $this->twig->render('Blog/home.twig.html', array(
            'messages' => $messages,
        ));
    }

...
----

Pour limiter l'affichage aux utilisateurs authentifiés nous utilisons la fonction `is_granted()` (fournie par le Twig bridge).

[source, html]
.views/Blog/home.html.twig
----
{% extends "layout.html.twig" %}

{% block content %}

    <h1>Accueil du microblog</h1>

    <div class="panel panel-default">
        <div class="panel-body">
    {% if messages %}
        {% for message in messages %}
            <article>
                <p>{{ message.text }}</p>
                <p>écrit le <time datetime="{{ message.date }}">{{ message.date|date("d/m/Y") }}</time> </p>
            {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                <div>
                    <a class="btn btn-warning" href="/messages/edit/{{ message.id }}" role="button">Modifier</a>
                    <a class="btn btn-danger" href="/messages/delete/{{ message.id }}" role="button">Supprimer</a>
                </div>
            {%  endif %}
            </article>
        {%  endfor %}

    {% else %}
        <p>Aucun message.</p>
    {%  endif %}
        </div>
    {% if is_granted('IS_AUTHENTICATED_FULLY') %}
        <div class="panel-footer">
            <a class="btn btn-default" href="/messages/new" role="button">Ajouter un message</a>
        </div>
    {%  endif %}
    </div>
{% endblock %}
----

Lancez l'application et vérifiez que la home est accessible sans être connecté(e).
Puis connectez-vous et vérifiez que l'url "/messages/list" affiche la même liste avec les boutons d'action en plus.

[NOTE]
====
La home n'utilise pas la même règle de firewall et ne nécessite pas d'être authentifié.
Comme l'utilisateur est anonyme il n'est pas possible de récupérer des informations sur l'utilisateur via la route `/` (règle de firewall).

Aussi pour palier à ce problème nous créons une seconde url qui est dans la règle de firewall `authenticated`.
====

Nous ajoutons un second utilisateur avec un rôle différent "ROLE_USER".

[source, php]
.app/app.php
----
...

// Configuration des droits de l'application
$app['security.firewalls'] = array(

    ...

    'secured' => array(
        'pattern' => '^.*$',
        'form' => array('login_path' => '/login'),
        'users' => array(
            'admin' => array('ROLE_ADMIN', '$2y$10$3i9/lVd8UOFIJ6PAMFt8gu3/r5g0qeCJvoSlLCsvMTythye19F77a'), // password 'foo'
            'user' => array('ROLE_USER', '$2y$10$3i9/lVd8UOFIJ6PAMFt8gu3/r5g0qeCJvoSlLCsvMTythye19F77a'), // password 'foo'
        ),
        'logout' => array('logout_path' => '/logout', 'invalidate_session' => true),
    ),
);
----

Connectez-vous et vérifiez que vous accédez à l'url /security/users/add.

Nous souhaiterions limiter la gestion des utilisateurs uniquement au rôle "ROLE_ADMIN".

Nous utilisons pour cela l'ACL (Access Control List) fourni par le `SecurityService`.

[source, php]
.app/app.php
----
$app->register(new Silex\Provider\SecurityServiceProvider());

$app['security.access_rules'] = array(
    array('^/security', 'ROLE_ADMIN'),
);
----

Avec cette règle l'accès à toute url qui commence par `/security` est limité aux utilisateurs connectés *et* qui disposent du rôle "ROLE_ADMIN".

Rechargez la page et vérifiez qu'elle n'est plus accessible qu'au rôle administrateur.

=== Dynamisation de la gestion des comptes

Notre système de connexion ne s'appuie pas sur les comptes que nous gérons.
Nous souhaitons maintenant nous appuyer sur les comptes créés dans la partie `/security`.

Créons un service qui utilise l'interface `UserProviderInterface` du component `Security` de Symfony.

[source, php]
.src/Security/Provider/UserSecurityProvider.php
----
<?php

namespace Security\Provider;


use Security\Repository\UserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserSecurityProvider implements UserProviderInterface
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        $user = $this->userRepository->fetchOneByEmail($username);
        if(!$user) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
        return new User($user->getEmail(), $user->getPassword(), array('ROLE_ADMIN'));
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }
}
----

La méthode qui nous importe pour gérer la connexion est `loadUserByUsername()`.

Elle permet de rechercher l'utilisateur et s'il existe de retourner un objet `Symfony\Component\Security\Core\User\User`.
Le constructeur de cette classe dispose de 2 paramètres obligatoires le `username` et le `password`.

Nous passons en troisième paramètre une liste de rôles dont l'utilisateur disposera si l'authentification réussit.

Dans notre cas nous nous appuyons sur le repository déjà créé pour déterminer si un utilisateur existe et connaitre l'empreinte de son mot de passe.

[NOTE]
====
Attention le password ne doit pas être en clair mais de la forme vue précédemment (I.e. "$2y$10$3i9/lVd8UOFIJ6PAMFt8gu3/r5g0qeCJvoSlLCsvMTythye19F77a").
====

Il nous reste à indiquer au `SecurityService` d'utiliser notre service pour gérer l'authentification.

[source, php]
.app/app.php
----
...

use Security\Provider\UserSecurityProvider;

...

$app['security.firewalls'] = array(
    'login' => array(
        'pattern' => '^/login$',
    ),
    'home' => array(
        'pattern' => '^/$',
    ),
    'secured' => array(
        'pattern' => '^.*$',
        'form' => array('login_path' => '/login'),
        'users' => function () use ($app) {
            return new UserSecurityProvider($app['security.user.repository']);
        },

        'logout' => array('logout_path' => '/logout', 'invalidate_session' => true),
    ),
);

...
----

== Rapports d'erreurs

La configuration des rapports d'erreurs joue également un rôle dans la sécurité de l'application.
Des données utiles lors du développement peuvent donner des informations sur la structure et le fonctionnement de l'application.

Voyons les directives suivantes.

* display_errors : Détermine si les erreurs sont affichées footnote:[http://php.net/manual/fr/errorfunc.configuration.php#ini.display-errors].
Pour la partie CLI il existe une valeur `stderr` pour renvoyer vers le flux standard d'erreur.
* display_startup_errors : Identique au précédent mais pendant la phase de démarrage footnote:[http://php.net/errorfunc.configuration#ini.display-startup-errors].
* error_reporting : Définit les erreurs prises en compte footnote:[http://php.net/errorfunc.configuration#ini.error-reporting].
* log_errors : Détermine si les erreurs sont écrites dans les journaux d'erreurs.

Voici 2 exemples de configurations une pour le développement et une pour la production.

[source]
.Développement
----
display_errors = On
display_startup_errors = On
error_reporting = -1
log_errors = On
----

[source]
.Production
----
display_errors = Off
display_startup_errors = Off
error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
log_errors = On
----

Pour illustrer le fonctionnement nous modifions le fichier de bootstrap de la façon suivante.

[source]
.web/index.php
----
<?php
//$app = require __DIR__ . '/../app/app.php';

$app->run();
----

Lancez le serveur web interne.

[source, bash]
----
$ php -S localhost:8080 -t web web/index.php
----

Vous devriez obtenir une page vide et des logs d'erreurs dans le terminal.
Dans le cas du serveur web interne le journal d'erreurs est tout simplement la sortie standard (stdout).

Éditez votre fichier de configuration vous devriez trouver les valeurs suivantes.

[source]
./etc/php/7.0/cli/php.ini
----
display_errors = Off
display_startup_errors = Off
error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
log_errors = On
----

Modifiez-les de la façon suivante.

[source]
./etc/php/7.0/cli/php.ini
----
display_errors = On
display_startup_errors = Off
error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
log_errors = Off
----

Redémarrez le serveur sans quoi ces modifications ne seront pas prises en compte.
. Faites CTRL+C pour arrêter le serveur.
. Modifiez la configuration.
. Relancez le serveur.

[NOTE]
====
Cette configuration n'a qu'un sens pédagogique ne l'utilisez pas.
====

Si vous rechargez la page vous constatez que les erreurs sont affichées dans le navigateur et plus dans le terminal.

Modifiez la directive `error_reporting` pour afficher uniquement les erreurs.

[source]
./etc/php/7.0/cli/php.ini
----
error_reporting = E_ERROR
----

Rechargez la page. 
L'erreur de niveau `Notice` a disparu. Il ne reste que l'erreur de niveau `Fatal`.

Pour finir remettez la configuration initiale.

[source]
./etc/php/7.0/cli/php.ini
----
display_errors = Off
display_startup_errors = Off
error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
log_errors = On
----

== Si vous êtes en avance

=== Hashing de mots de passe

Pour renforcer la sécurité de l'empreinte calculer on utilise un "salt" qui est ajouté au mot de passe. 
Ceci permet de ne pas avoir la même empreinte informations identiques footnote:[https://fr.wikipedia.org/wiki/Salage_(cryptographie)].

Un salt est généré aléatoirement par la fonction `password_hash`.
La chaine de caractères retournée précise en plus du hash le "salt" utilisé. 
Il sera nécessaire à la fonction `password_verify`.

La fonction `password_hash` permet de passer en option un "salt".

Modifions notre test avec un "salt" donné uniquement à titre d'exemple. Évitez d'utiliser un "salt" qui ne soit pas généré aléatoirement.

[source, php]
.tests/Hash/HashingPassword.php
----
 ...
    /**
     * @beforeClass
     */
    public static function init() {
        self::$passwordHash = password_hash('mon mot de passe', PASSWORD_DEFAULT, array('salt' => 'azertyuiopqsdfghjklmwx'));
        var_dump(self::$passwordHash);
    }
 ...
----

Lancez le test unitaire. 
Vous voyez le "salt" dans la chaine de caractères (il manque le dernier caractère).

[source, bash]
----
$ vendor/bin/phpunit tests/Hash/HashingPassword.php
PHPUnit 5.5.4 by Sebastian Bergmann and contributors.

string(60) "$2y$10$azertyuiopqsdfghjklmwuad5mSxpDfxsBw87EoWQP.A/TP3DA3pi"
..                                                                  2 / 2 (100%)

Time: 216 ms, Memory: 3.00MB

OK (2 tests, 2 assertions)
----

=== Gestion des droits

* Ajoutez une édition et une suppression des utilisateurs.
* Ajoutez à la gestion des utilisateurs l'association d'un ou plusieurs rôles.
* Modifiez la connexion d'un utilisateur pour charger ses différents rôles.
* Faire en sorte que security soit accessible uniquement aux administrateurs et l'édition de messages uniquement aux utilisateurs.
* Ajoutez la notion d'auteur aux messages.
* Limitez l'édition aux messages dont l'utilisateur est l'auteur.

== Pour aller plus loin

=== Validation

* Documentation du component symfony/validator http://symfony.com/doc/master/validation.html (en anglais)

=== Authentification

* Documentation du component symfony/security http://symfony.com/doc/2.8/security.html
