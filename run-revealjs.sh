#!/usr/bin/env bash

revealPath=~/tmp/reveal.js

docker run -p 9000:9000  -v "$revealPath":/usr/src/app -w /usr/src/app node:12 npm start  -- --port=9000

